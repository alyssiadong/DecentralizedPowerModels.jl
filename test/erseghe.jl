@testset "instantiate Erseghe decentralized model" begin
    @testset "5 bus - 3 regions ACP Model testcase" begin
        file = "../test/data/matpower/case5_zones.m"
        pm_dec = instantiate_model_dec_erseghe(file, ACPPowerModel, ipopt_solver)

        @test pm_dec isa ErsegheDS
        @test length(pm_dec.pms) == 3
        @test collect(keys(ref(pm_dec, 10, :region))) == [10]
        @test length( ref(pm_dec, 10, :region, 10, "arcs") ) == 7
        @test in( (1,2,1), ref(pm_dec, 20, :region, 20, "out_arcs_to") )
        @test !in( (6,3,4), ref(pm_dec, 10, :region, 10, "arcs") )
        @test (ref(pm_dec, 20, :region, 20, "in_arcs_from") == [(4,2,3)]) && 
            (ref(pm_dec, 20, :region, 20, "out_arcs_to") == [(1,2,1)])
        @test (length(ref(pm_dec,10,:bus)) == 5) && 
            (length(ref(pm_dec,20,:bus)) == 4) && 
            (length(ref(pm_dec,30,:bus)) == 3)
        @test sum([v for ((k,h),v) in ref(pm_dec, 10, :bus, 4, "param_a") if k==10]) == ref(pm_dec, 10, :bus, 4, "param_d")[10]
    end

    @testset "5 bus - 3 regions ACR Model testcase" begin
        file = "../test/data/matpower/case5_zones.m"
        pm_dec = instantiate_model_dec_erseghe(file, ACRPowerModel, ipopt_solver)

        @test pm_dec isa ErsegheDS
        @test length(pm_dec.pms) == 3
        @test collect(keys(ref(pm_dec, 10, :region))) == [10]
        @test length( ref(pm_dec, 10, :region, 10, "arcs") ) == 7
        @test in( (1,2,1), ref(pm_dec, 20, :region, 20, "out_arcs_to") )
        @test !in( (6,3,4), ref(pm_dec, 10, :region, 10, "arcs") )
        @test (ref(pm_dec, 20, :region, 20, "in_arcs_from") == [(4,2,3)]) && 
            (ref(pm_dec, 20, :region, 20, "out_arcs_to") == [(1,2,1)])
        @test (length(ref(pm_dec,10,:bus)) == 5) && 
            (length(ref(pm_dec,20,:bus)) == 4) && 
            (length(ref(pm_dec,30,:bus)) == 3)
        @test sum([v for ((k,h),v) in ref(pm_dec, 10, :bus, 4, "param_a") if k==10]) == ref(pm_dec, 10, :bus, 4, "param_d")[10]
    end

    @testset "57 bus - 8 regions testcase" begin
        file = "../test/data/matpower/case57.m"
        pm_dec = instantiate_model_dec_erseghe(file, ACPPowerModel, ipopt_solver)

        @test pm_dec isa ErsegheDS
        @test length(pm_dec.pms) == 8
        @test in(13, ref(pm_dec, 5,:region,5,"bus"))
        @test length(ref(pm_dec, 5,:region,5,"bus")) == 7
    end
end

@testset "opf Erseghe decentralized model" begin
    @testset "5 bus testcase" begin
        file = "../test/data/matpower/case5_zones.m"
        result,monitor = run_dec_opf_erseghe(file, ACPPowerModel, ipopt_solver;monitor_max_time=200)
        
        @test all([v for (k,v) in result["termination_status"]] .== _MOI.LOCALLY_SOLVED)
        @test isapprox(result["objective"], 182; atol=2e0)
    end

    @testset "9 bus testcase" begin
        file = "../test/data/matpower/case9_zones.m"
        result,monitor = run_dec_opf_erseghe(file, ACPPowerModel, ipopt_solver;monitor_max_time=200)
        
        @test all([v for (k,v) in result["termination_status"]] .== _MOI.LOCALLY_SOLVED)
        @test isapprox(result["objective"], 347; atol=2e0)
    end

    @testset "30 bus testcase" begin
        file = "../test/data/matpower/case30.m"
        result,monitor = run_dec_opf_erseghe(file, ACPPowerModel, ipopt_solver;monitor_max_time=400)
        
        @test all([v for (k,v) in result["termination_status"]] .== _MOI.LOCALLY_SOLVED)
        @test isapprox(result["objective"], 2.967; atol=1e-1)
    end
end

@testset "result output" begin
    @testset "9 bus testcase - ACP model" begin
        file = "../test/data/matpower/case9_zones.m"
        result,monitor = run_dec_opf_erseghe(file, ACPPowerModel, ipopt_solver;
            monitor_max_time=20,monitoring=false)

        @test haskey(result, "branch")
        @test haskey(result, "bus")
        @test haskey(result, "gen")
        @test haskey(result, "objective")
        @test !haskey(result, "solution")
        @test length(result["bus"])==9
        @test haskey(result["bus"]["1"], "vm")
    end

    @testset "9 bus testcase - ACR model" begin
        file = "../test/data/matpower/case9_zones.m"
        result,monitor = run_dec_opf_erseghe(file, ACRPowerModel, ipopt_solver;
            monitor_max_time=20,monitoring=false)

        @test haskey(result, "branch")
        @test haskey(result, "bus")
        @test haskey(result, "gen")
        @test haskey(result, "objective")
        @test !haskey(result, "solution")
        @test length(result["bus"])==9
        @test haskey(result["bus"]["1"], "vr")
    end
end