using DecentralizedPowerModels
import PowerModels
import PowerModels: AbstractPowerModel, ACRPowerModel, ACPPowerModel, optimize_model!
import PowerModels: ref

import InfrastructureModels

import Memento

# Suppress warnings during testing.
Memento.setlevel!(Memento.getlogger(PowerModels), "error")
Memento.setlevel!(Memento.getlogger(InfrastructureModels), "error")
DecentralizedPowerModels.logger_config!("error")

import MathOptInterface 
const _MOI = MathOptInterface

import JuMP
import Ipopt

import LinearAlgebra
import SparseArrays
using Test

# default setup for solvers
ipopt_solver = JuMP.optimizer_with_attributes(Ipopt.Optimizer, "tol"=>1e-6, "print_level"=>0)

@testset "DecentralizedPowerModels.jl" begin
    include("erseghe.jl")
    include("b2b.jl")
end
