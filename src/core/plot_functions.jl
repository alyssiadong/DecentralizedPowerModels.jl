import Plots
import StatsPlots
"
Monitor Gif
"
function gif_monitor(global_monitor::Dict, result_ac_opf::Dict; xrange=(0.9,1.1), yrange=(-0.25,0.3), name::String="test.gif")
    @assert endswith(name, ".gif") "Name must end with .gif"
    conversion!(result_ac_opf)
    conversion!(global_monitor)
    anim = Plots.@animate for k in 1:length(global_monitor["bus"]["1"]["vr"])
        compare_bus(result_ac_opf, global_monitor, k, xrange, yrange)
    end every 10
    Plots.gif(anim, name, fps = 15)
end

function gif_monitor_gen_active(global_monitor::Dict, result_ac_opf::Dict; xrange=(0.9,1.1), yrange=(-0.25,7), name::String="test.gif")
    @assert endswith(name, ".gif") "Name must end with .gif"
    anim = Plots.@animate for k in 1:length(global_monitor["gen"]["1"]["pg"])
        compare_gen_active(result_ac_opf, global_monitor, k, xrange, yrange)
    end every 10
    Plots.gif(anim, name, fps = 15)
end
function gif_monitor_gen_active(ds::DecentralizedSystem, global_monitor::Dict, result_ac_opf::Dict; xrange=(0.9,1.1), yrange=(-0.25,7), name::String="test.gif")
    @assert endswith(name, ".gif") "Name must end with .gif"
    anim = Plots.@animate for k in 1:length(global_monitor["gen"]["1"]["pg"])
        compare_gen_active(ds, result_ac_opf, global_monitor, k, xrange, yrange)
    end every 10
    Plots.gif(anim, name, fps = 15)
end

function gif_monitor_gen_reactive(global_monitor::Dict, result_ac_opf::Dict; xrange=(0.9,1.1), yrange=(-7,7), name::String="test.gif")
    @assert endswith(name, ".gif") "Name must end with .gif"
    anim = Plots.@animate for k in 1:length(global_monitor["gen"]["1"]["pg"])
        compare_gen_reactive(result_ac_opf, global_monitor, k, xrange, yrange)
    end every 10
    Plots.gif(anim, name, fps = 15)
end
function gif_monitor_gen_reactive(ds::DecentralizedSystem, global_monitor::Dict, result_ac_opf::Dict; xrange=(0.9,1.1), yrange=(-7,7), name::String="test.gif")
    @assert endswith(name, ".gif") "Name must end with .gif"
    anim = Plots.@animate for k in 1:length(global_monitor["gen"]["1"]["pg"])
        compare_gen_reactive(ds, result_ac_opf, global_monitor, k, xrange, yrange)
    end every 10
    Plots.gif(anim, name, fps = 15)
end

function compare_bus(result_ac_opf::Dict, result_dec_opf::Dict, k::Int, xrange, yrange)
    ids = Int32[]
    vi_ac_opf = Float64[]
    vr_ac_opf = Float64[]
    vi_dec_opf = Float64[]
    vr_dec_opf = Float64[]
    for (idbus, bus) in result_ac_opf["bus"]
        push!(ids, parse(Int,idbus))
        push!(vr_ac_opf, bus["vr"])
        push!(vi_ac_opf, bus["vi"])
        push!(vr_dec_opf, result_dec_opf["bus"][idbus]["vr"][k])
        push!(vi_dec_opf, result_dec_opf["bus"][idbus]["vi"][k])
    end

    p = Plots.scatter(vr_ac_opf, vi_ac_opf,   label="ref", markershape = :circle, xlims=xrange, ylims=yrange)
    Plots.scatter!(p, vr_dec_opf, vi_dec_opf, label="dec", markershape = :cross, xlims=xrange, ylims=yrange)

    p
end

function compare_gen_active(result_ac_opf::Dict, result_dec_opf::Dict, k::Int, xrange, yrange)
    ids = Int32[]
    pg_ac_opf = Float64[]
    pg_dec_opf = Float64[]
    for (idgen, gen) in result_ac_opf["gen"]
        push!(ids, parse(Int,idgen))
        push!(pg_ac_opf, gen["pg"])
        push!(pg_dec_opf, result_dec_opf["gen"][idgen]["pg"][k])
    end

    p = Plots.bar(pg_ac_opf,    label="ref",    ylims=yrange)
    Plots.bar!(pg_dec_opf,      label="dec",    ylims=yrange)

    p
end
function compare_gen_active(ds::DecentralizedSystem, result_ac_opf::Dict, result_dec_opf::Dict, k::Int, xrange, yrange)
    ids = String[]
    pg_ac_opf = Float64[]
    pg_dec_opf = Float64[]
    region = String[]
    for (kreg,lpm) in ds.pms
        for (idgen, gen) in ref(lpm, :gen)
            push!(ids, string(idgen))
            push!(pg_ac_opf, result_ac_opf["gen"][string(idgen)]["pg"])
            push!(pg_dec_opf, result_dec_opf["gen"][string(idgen)]["pg"][k])
            push!(region, string(kreg))
        end
    end

    y = hcat(pg_ac_opf, pg_dec_opf)

    p = StatsPlots.groupedbar( region,   pg_dec_opf,   group=ids,
        label="ref",    ylims=yrange,  
        legend = false, bar_width = 0.67)
    p
end

function compare_gen_reactive(result_ac_opf::Dict, result_dec_opf::Dict, k::Int, xrange, yrange)
    ids = Int32[]
    qg_ac_opf = Float64[]
    qg_dec_opf = Float64[]
    for (idgen, gen) in result_ac_opf["gen"]
        push!(ids, parse(Int,idgen))
        push!(qg_ac_opf, gen["qg"])
        push!(qg_dec_opf, result_dec_opf["gen"][idgen]["qg"][k])
    end

    p = Plots.bar(qg_ac_opf,    label="ref",    markershape = :circle, ylims=yrange)
    Plots.bar!(qg_dec_opf,      label="dec",    markershape = :cross, ylims=yrange)

    p
end
function compare_gen_reactive(ds::DecentralizedSystem, result_ac_opf::Dict, result_dec_opf::Dict, k::Int, xrange, yrange)
    ids = String[]
    qg_ac_opf = Float64[]
    qg_dec_opf = Float64[]
    region = String[]
    for (kreg,lpm) in ds.pms
        for (idgen, gen) in ref(lpm, :gen)
            push!(ids, string(idgen))
            push!(qg_ac_opf, result_ac_opf["gen"][string(idgen)]["qg"])
            push!(qg_dec_opf, result_dec_opf["gen"][string(idgen)]["qg"][k])
            push!(region, string(kreg))
        end
    end

    y = hcat(qg_ac_opf, qg_dec_opf)

    p = StatsPlots.groupedbar( region,   qg_dec_opf,   group=ids,
        label="ref",    ylims=yrange,  
        legend = false, bar_width = 0.67)
    p
end

function polar_to_rectangular_conversion!(result_dic::Dict)
    for (idbus, bus) in result_dic["bus"]
        vm = bus["vm"]
        va = bus["va"]

        v = vm.*exp.(im*va)
        vr = real.(v)
        vi = imag.(v)
        bus["vr"] = vr
        bus["vi"] = vi
    end
end

function rectangular_to_polar_conversion!(result_dic::Dict)
    for (idbus, bus) in result_dic["bus"]
        vr = bus["vr"]
        vi = bus["vi"]

        v = vr.+im .*vi
        vm = abs.(v)
        va = angle.(v)
        bus["vm"] = vm
        bus["va"] = va
    end
end

function conversion!(result_dic::Dict)
    bus_dic = first(result_dic["bus"])[2]
    if haskey(bus_dic, "vm") & !haskey(bus_dic, "vr")
        polar_to_rectangular_conversion!(result_dic)
    elseif haskey(bus_dic, "vr") & !haskey(bus_dic, "vm")
        rectangular_to_polar_conversion!(result_dic)
    end
end


"
Decomposition visualization
"

using LightGraphs, SimpleWeightedGraphs
using Gadfly, GraphPlot, Colors
using PlotlyJS

function regionsViz(file::String)
    pm = PowerModels.instantiate_model(file, PowerModels.ACPPowerModel, 
        PowerModels.build_opf;
        ref_extensions=[ref_add_regions!, ref_add_admm_params!, #])
        ref_add_starting_voltage_value!, ref_add_starting_genpower_value!])

    Nreg = length(PowerModels.ref(pm, :region))
    outerbus = Int[]
    for (nreg, reg) in PowerModels.ref(pm, :region)
        push!(outerbus, reg[:outer_neigh_bus]...)
    end
    outerbus = unique(outerbus)
    Nouterbus = length(outerbus)

    vertices = vcat([string("R", nreg) for nreg in 1:Nreg], 
                        [string("b", bus) for bus in outerbus])
    nodesize = vcat([20 for nreg in 1:Nreg], 
                        [10 for bus in outerbus])
    nodecolor = vcat([colorant"crimson" for nreg in 1:Nreg], 
                        [colorant"moccasin" for bus in outerbus])

    gr = SimpleGraph(Nreg+Nouterbus)
    for (nreg, reg) in PowerModels.ref(pm, :region)
        for (l, link) in reg[:out_buspairs_collection]
            bus = link["bus"]
            bus_vertice = Nreg + findfirst(outerbus.==bus)
            add_edge!(gr, nreg, bus_vertice)
        end
    end

    gplot(gr, 
            nodefillc=nodecolor, 
            nodelabel=vertices,
            nodesize =nodesize ,
            )
end

function linksViz(fileOrDs)
    dic = linksDict(fileOrDs)
    regs = collect(keys(dic))
    links = collect(values(dic))
    PlotlyJS.plot(PlotlyJS.bar(;x = regs, y=links, title="Number of links per region"))
end
function linksRegionViz(fileOrDs)
    dic = linksRegionDict(fileOrDs)
    regs = collect(keys(dic))
    links = collect(values(dic))
    PlotlyJS.plot(PlotlyJS.bar(;x = regs, y=links, title="Number of links per region"))
end

function linksDict(file::String)
    # Returns a dictionary Dict( region => number of outward links for that region ) 
    pm = PowerModels.instantiate_model(file, PowerModels.ACPPowerModel, 
        PowerModels.build_opf;
        ref_extensions=[ref_add_regions!, ref_add_admm_params!, #])
        ref_add_starting_voltage_value!, ref_add_starting_genpower_value!])

    regs = Int[]
    links = Int[]
    dic = Dict{Int,Int}()
    for (nreg, reg) in PowerModels.ref(pm, :region)
        dic[nreg] = length(reg[:out_arcs])
    end
    return dic
end
function linksDict(ds::DecentralizedSystem)
    # Returns a dictionary Dict( region => number of outward links for that region ) 
    regs = Int[]
    links = Int[]
    dic = Dict{Int,Int}()
    for (nreg, pm) in ds.pms
        dic[nreg] = length(PowerModels.ref(pm, :region, nreg)["out_arcs"])
    end
    return dic
end

function linksRegionDict(file::String)
    # Returns a dictionary Dict( region => number of neighbor regions ) 
    pm = PowerModels.instantiate_model(file, PowerModels.ACPPowerModel, 
        PowerModels.build_opf;
        ref_extensions=[ref_add_regions!, ref_add_admm_params!, #])
        ref_add_starting_voltage_value!, ref_add_starting_genpower_value!])

    dic = Dict{Int,Int}()
    for (nreg, reg) in PowerModels.ref(pm, :region)
        neigh = Int[]
        for (k,h,bus) in reg[:out_regionpairs_collection]
            push!(neigh, h)
        end
        unique!(neigh)
        dic[nreg] = length(neigh)
    end
    return dic
end
function linksRegionDict(ds::DecentralizedSystem)
    # Returns a dictionary Dict( region => number of neighbor regions ) 
    dic = Dict{Int,Int}()
    for (nreg, pm) in ds.pms
        neigh = Int[]
        for (k,h,bus) in PowerModels.ref(pm, :region, nreg)["out_regionpairs_collection"]
            push!(neigh, h)
        end
        unique!(neigh)
        dic[nreg] = length(neigh)
    end
    return dic
end

function listRegionNeighbors(ds::DecentralizedSystem)
    # Dict(region => [list of region neighbors])
    dic = Dict{Int,Any}()
    for (nreg, pm) in ds.pms
        neigh = Int[]
        for (k,h,bus) in PowerModels.ref(pm, :region, nreg)["out_regionpairs_collection"]
            push!(neigh, h)
        end
        unique!(neigh)
        dic[nreg] = neigh
    end
    return dic
end