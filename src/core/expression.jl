function expression_solution_comparison(pm::AbstractACPModel; nw::Int=nw_id_default, report::Bool=true)
    # For monitoring purposes
	if !haskey(first(ref(pm,nw,:branch))[2],"optimal")
		return
	else
	    current_region = collect(ids(pm, nw, :region))[1]
	    buses = ref(pm, nw, :region, current_region,"bus")
	    arcs_from = ref(pm, nw, :region, current_region, "arcs_from")
	    arcs_to = ref(pm, nw, :region, current_region, "arcs_to")
	    gens = ids(pm, nw, :gen)

	    vm = var(pm, nw, :vm)
	    va = var(pm, nw, :va)

	    gen_comp = 		JuMP.@expression( pm.model, sum(	(var(pm, nw, :pg, k) - ref(pm,nw, :gen, k,"optimal")["pg"])^2 + 	
				    										(var(pm, nw, :qg, k) - ref(pm,nw, :gen, k,"optimal")["qg"])^2
				    									for k in gens ) )
	    branch_comp = 	JuMP.@expression( pm.model, sum(	(var(pm, nw, :p, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["pf"])^2 +	
				    										(var(pm, nw, :q, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["qf"])^2
				    									for (l,i,j) in arcs_from ) +
	    											sum(	(var(pm, nw, :p, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["pt"])^2 +	
				    										(var(pm, nw, :q, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["qt"])^2
				    									for (l,i,j) in arcs_to ) )
	    voltage_comp = 	JuMP.@NLexpression(pm.model, sum(	(vm[j]*cos(va[j]) - ref(pm,nw, :bus, j,"optimal")["vr"])^2 +
				    										(vm[j]*sin(va[j]) - ref(pm,nw, :bus, j,"optimal")["vi"])^2
				    									for j in buses) )

	    if report
	    	sol(pm, nw, :bus,0)[:comp_optimal] = voltage_comp
	    	sol(pm, nw, :branch,0)[:comp_optimal] = branch_comp
	    	if length(gens)>0
	    		sol(pm, nw, :gen,0)[:comp_optimal] = gen_comp
	    	end
	    end
	end
end
function expression_solution_comparison(pm::AbstractACRModel; nw::Int=nw_id_default, report::Bool=true)
    # For monitoring purposes
	if !haskey(first(ref(pm,nw,:branch))[2],"optimal")
		return
	else
	    current_region = collect(ids(pm, nw, :region))[1]
	    buses = ref(pm, nw, :region, current_region,"bus")
	    arcs_from = ref(pm, nw, :region, current_region, "arcs_from")
	    arcs_to = ref(pm, nw, :region, current_region, "arcs_to")
	    gens = ids(pm, nw, :gen)

	    gen_comp = 		JuMP.@expression( pm.model, sum(	(var(pm, nw, :pg, k) - ref(pm,nw, :gen, k,"optimal")["pg"])^2 + 	
				    										(var(pm, nw, :qg, k) - ref(pm,nw, :gen, k,"optimal")["qg"])^2
				    									for k in gens ) )
	    branch_comp = 	JuMP.@expression( pm.model, sum(	(var(pm, nw, :p, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["pf"])^2 +	
				    										(var(pm, nw, :q, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["qf"])^2
				    									for (l,i,j) in arcs_from ) +
	    											sum(	(var(pm, nw, :p, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["pt"])^2 +	
				    										(var(pm, nw, :q, (l,i,j)) - ref(pm,nw, :branch, l,"optimal")["qt"])^2
				    									for (l,i,j) in arcs_to ) )
	    voltage_comp = 	JuMP.@expression(pm.model, 	sum(	(var(pm, nw, :vr, j) - ref(pm,nw, :bus, j,"optimal")["vr"])^2 +
				    										(var(pm, nw, :vi, j) - ref(pm,nw, :bus, j,"optimal")["vi"])^2
				    									for j in buses) )

	    if report
	    	sol(pm, nw, :bus,0)[:comp_optimal] = voltage_comp
	    	sol(pm, nw, :branch,0)[:comp_optimal] = branch_comp
	    	if length(gens)>0
	    		sol(pm, nw, :gen,0)[:comp_optimal] = gen_comp
	    	end
	    end
	end
end