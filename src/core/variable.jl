function variable_bus_voltage_dec(pm::AbstractACPModel; kwargs...)
    variable_bus_voltage_dec_magnitude(pm; kwargs...)
    variable_bus_voltage_dec_angle(pm; kwargs...)
end
function variable_bus_voltage_dec(pm::AbstractACRModel; kwargs...)
    variable_bus_voltage_dec_real(pm; kwargs...)
    variable_bus_voltage_dec_imaginary(pm; kwargs...)
end

"variable: `t[i]` for `i` in `bus`es"
function variable_bus_voltage_dec_angle(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    va = var(pm, nw)[:va] = JuMP.@variable(pm.model,
        [i in ref(pm, nw, :region, current_region, "bus")], base_name="$(nw)_va",
        start = comp_start_value(ref(pm, nw, :bus, i), "va_start")
    )

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :va, ref(pm, nw, :region, current_region, "bus"), va)
end

"variable: `v[i]` for `i` in `bus`es"
function variable_bus_voltage_dec_magnitude(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    vm = var(pm, nw)[:vm] = JuMP.@variable(pm.model,
        [i in ref(pm, nw, :region, current_region, "bus")], base_name="$(nw)_vm",
        start = comp_start_value(ref(pm, nw, :bus, i), "vm_start", 1.0)
    )

    if bounded
        for (i, bus) in ref(pm, nw, :bus)
            if i∈ref(pm, nw, :region, current_region, "bus")
                JuMP.set_lower_bound(vm[i], bus["vmin"])
                JuMP.set_upper_bound(vm[i], bus["vmax"])
            end
        end
    end

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :vm, ref(pm, nw, :region, current_region, "bus"), vm)
end


"real part of the voltage variable `i` in `bus`es"
function variable_bus_voltage_dec_real(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    vr = var(pm, nw)[:vr] = JuMP.@variable(pm.model,
        [i in ref(pm, nw, :region, current_region, "bus")], base_name="$(nw)_vr",
        start = comp_start_value(ref(pm, nw, :bus, i), "vr_start", 1.0)
    )

    if bounded
        for (i, bus) in ref(pm, nw, :bus)
            if i∈ref(pm, nw, :region, current_region, "bus")
                JuMP.set_lower_bound(vr[i], -bus["vmax"])
                JuMP.set_upper_bound(vr[i],  bus["vmax"])
            end
        end
    end

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :vr, ref(pm, nw, :region, current_region, "bus"), vr)
end

"real part of the voltage variable `i` in `bus`es"
function variable_bus_voltage_dec_imaginary(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    vi = var(pm, nw)[:vi] = JuMP.@variable(pm.model,
        [i in ref(pm, nw, :region, current_region, "bus")], base_name="$(nw)_vi",
        start = comp_start_value(ref(pm, nw, :bus, i), "vi_start")
    )

    if bounded
        for (i, bus) in ref(pm, nw, :bus)
            if i∈ref(pm, nw, :region, current_region, "bus")
                JuMP.set_lower_bound(vi[i], -bus["vmax"])
                JuMP.set_upper_bound(vi[i],  bus["vmax"])
            end
        end
    end

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :vi, ref(pm, nw, :region, current_region, "bus"), vi)
end









"
Creates branch variables for the decentralized problem.
Only creates branch variable on the side of the principal bus. 
Used in Erseghe and B2B and R2R.
"
function variable_branch_power_dec(pm::AbstractPowerModel; kwargs...)
    variable_branch_power_dec_real(pm; kwargs...)
    variable_branch_power_dec_imaginary(pm; kwargs...)
end

"variable: `p[l,i,j]` for `(l,i,j)` in `arcs`"
function variable_branch_power_dec_real(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    p = var(pm, nw)[:p] = JuMP.@variable(pm.model,
        [(l,i,j) in ref(pm, nw, :region, current_region,"arcs")], base_name="$(nw)_p",
        start = comp_start_value(ref(pm, nw, :branch, l), "p_start")
    )

    if bounded
        flow_lb, flow_ub = ref_calc_branch_flow_bounds(ref(pm, nw, :branch), ref(pm, nw, :bus))

        for arc in ref(pm, nw, :region, current_region, "arcs")
            l,i,j = arc
            if !isinf(flow_lb[l])
                JuMP.set_lower_bound(p[arc], flow_lb[l])
            end
            if !isinf(flow_ub[l])
                JuMP.set_upper_bound(p[arc], flow_ub[l])
            end
        end
    end

    for (l,branch) in ref(pm, nw, :branch)
        if haskey(branch, "pf_start")
            f_idx = (l, branch["f_bus"], branch["t_bus"])
            if f_idx in ref(pm, nw, :region, current_region, "arcs_from")
                JuMP.set_start_value(p[f_idx], branch["pf_start"])
            end
        end
        if haskey(branch, "pt_start")
            t_idx = (l, branch["t_bus"], branch["f_bus"])
            if t_idx ref(pm, nw, :region, current_region, "arcs_to")
                JuMP.set_start_value(p[t_idx], branch["pt_start"])
            end
        end
    end
    report && _IM.sol_component_value_edge(pm, pm_it_sym, nw, :branch, :pf, :pt, ref(pm, nw, :region, current_region, "arcs_from"), ref(pm, nw, :region, current_region, "arcs_to"), p)
end
"variable: `q[l,i,j]` for `(l,i,j)` in `arcs`"
function variable_branch_power_dec_imaginary(pm::AbstractPowerModel; nw::Int=nw_id_default, bounded::Bool=true, report::Bool=true)
    current_region = collect(ids(pm, nw, :region))[1]
    q = var(pm, nw)[:q] = JuMP.@variable(pm.model,
        [(l,i,j) in ref(pm, nw, :region, current_region,"arcs")], base_name="$(nw)_q",
        start = comp_start_value(ref(pm, nw, :branch, l), "q_start")
    )

    if bounded
        flow_lb, flow_ub = ref_calc_branch_flow_bounds(ref(pm, nw, :branch), ref(pm, nw, :bus))

        for arc in ref(pm, nw, :region, current_region, "arcs")
            l,i,j = arc
            if !isinf(flow_lb[l])
                JuMP.set_lower_bound(q[arc], flow_lb[l])
            end
            if !isinf(flow_ub[l])
                JuMP.set_upper_bound(q[arc], flow_ub[l])
            end
        end
    end

    for (l,branch) in ref(pm, nw, :branch)
        if haskey(branch, "qf_start")
            f_idx = (l, branch["f_bus"], branch["t_bus"])
            if f_idx in ref(pm, nw, :region, current_region, "arcs_from")
                JuMP.set_start_value(q[f_idx], branch["qf_start"])
            end
        end
        if haskey(branch, "qt_start")
            t_idx = (l, branch["t_bus"], branch["f_bus"])
            if t_idx in ref(pm, nw, :region, current_region, "arcs_to")
                JuMP.set_start_value(q[t_idx], branch["qt_start"])
            end
        end
    end

    report && _IM.sol_component_value_edge(pm, pm_it_sym, nw, :branch, :qf, :qt, ref(pm, nw, :region, current_region, "arcs_from"), ref(pm, nw, :region, current_region, "arcs_to"), q)
end


