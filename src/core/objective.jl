""
function objective_min_fuel_cost_dec(pm::AbstractPowerModel; kwargs...)
    model = PowerModels.check_gen_cost_models(pm)

    if model == nothing
        return objective_min_fuel_cost_dec_zero(pm; kwargs...)
    elseif model == 2
        return objective_min_fuel_cost_dec_polynomial(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only cost models of types nothing and 2 are supported at this time, given cost model type of $(model)")
    end
end

""
function objective_min_fuel_cost_dec_polynomial(pm::AbstractPowerModel; kwargs...)
    order = PowerModels.calc_max_cost_index(pm.data)-1

    if order <= 2
        return _objective_min_fuel_cost_dec_polynomial_linquad(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only linear or quadratic models are supported at this time, given cost model order $(model)")
    end
end

""
function _objective_min_fuel_cost_dec_polynomial_linquad(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )

            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1])
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]*pg + gen["cost"][2])
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        for (j,bus) in nw_ref[:bus]
            vm = var(pm, n, :vm, j)
            va = var(pm, n, :va, j)

            betam = var(pm, n, :beta_m, j)             # parameter
            betaa = var(pm, n, :beta_a, j)             # parameter

            d = ref(pm, n, :bus, j)["param_d"][current_region]

            voltage_copy[(n,j)] = JuMP.@NLexpression(pm.model, 
                d*( (vm*cos(va)-betam*cos(betaa))^2 +
                    (vm*sin(va)-betam*sin(betaa))^2) )
        end
    end

    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,j)] for (j,bus) in nw_ref[:bus] )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_dec_polynomial_linquad(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )

            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1])
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]*pg + gen["cost"][2])
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        for (j,bus) in nw_ref[:bus]
            vr = var(pm, n, :vr, j)
            vi = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, j)             # parameter
            betai = var(pm, n, :beta_i, j)             # parameter

            d = ref(pm, n, :bus, j)["param_d"][current_region]

            voltage_copy[(n,j)] =  JuMP.@NLexpression(pm.model,
                d*((vr - betar)^2 + (vi - betai)^2))
        end
    end

    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,j)] for (j,bus) in nw_ref[:bus] )
        for (n, nw_ref) in nws(pm))
    )
end

""
function objective_min_fuel_cost_dec_zero(pm::AbstractPowerModel; kwargs...)
    _objective_min_fuel_cost_dec_zero(pm; kwargs...)
end
function _objective_min_fuel_cost_dec_zero(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        for (j,bus) in nw_ref[:bus]
            vm = var(pm, n, :vm, j)
            va = var(pm, n, :va, j)

            betam = var(pm, n, :beta_m, j)             # parameter
            betaa = var(pm, n, :beta_a, j)             # parameter

            d = ref(pm, n, :bus, j)["param_d"][current_region]

            voltage_copy[(n,j)] = JuMP.@NLexpression(pm.model, 
                d*( (vm*cos(va)-betam*cos(betaa))^2 +
                    (vm*sin(va)-betam*sin(betaa))^2) )
        end
    end
    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,j)] for (j,bus) in nw_ref[:bus] )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_dec_zero(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        for (j, bus) in nw_ref[:bus]
            vr = var(pm, n, :vr, j)
            vi = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, j)             # parameter
            betai = var(pm, n, :beta_i, j)             # parameter

            d = ref(pm, n, :bus, j)["param_d"][current_region]

            voltage_copy[(n,j)] = JuMP.@NLexpression(pm.model,
                d*((vr - betar)^2 + (vi - betai)^2))
        end
    end

    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,j)] for (j,bus) in nw_ref[:bus] )
        for (n, nw_ref) in nws(pm))
    )
end








""
function objective_min_fuel_cost_b2b(pm::AbstractPowerModel; kwargs...)
    model = PowerModels.check_gen_cost_models(pm)

    if model == nothing
        return objective_min_fuel_cost_b2b_zero(pm; kwargs...)
    elseif model == 2
        return objective_min_fuel_cost_b2b_polynomial(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only cost models of types nothing and 2 are supported at this time, given cost model type of $(model)")
    end
end

""
function objective_min_fuel_cost_b2b_polynomial(pm::AbstractPowerModel; kwargs...)
    order = PowerModels.calc_max_cost_index(pm.data)-1

    if order <= 2
        return _objective_min_fuel_cost_b2b_polynomial_linquad(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only linear or quadratic models are supported at this time, given cost model order $(model)")
    end
end

""
function _objective_min_fuel_cost_b2b_polynomial_linquad(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    A = ref(pm, :norm_coef)
    reg_coef = ref(pm, :region, current_region, "weight_b2b")

    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )
            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]/A*reg_coef)
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, (gen["cost"][1]*pg + gen["cost"][2])/A*reg_coef)
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, (gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])/A*reg_coef)
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        collection = ref(pm, n, :region, current_region,"out_regionpairs_collection")
        for (reg1,reg2,j) in collection
            vm_j = var(pm, n, :vm, j)
            va_j = var(pm, n, :va, j)

            betar = var(pm, n, :beta_r, (reg1,reg2,j))
            betai = var(pm, n, :beta_i, (reg1,reg2,j))

            voltage_copy[(n,(reg1,reg2,j))] = JuMP.@NLexpression(pm.model, 
                ρ/2*( betar - vm_j*cos(va_j))^2 +
                ρ/2*( betai - vm_j*sin(va_j))^2
                )
        end
    end

    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_regionpairs_collection") )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_b2b_polynomial_linquad(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    A = ref(pm, :norm_coef)
    reg_coef = ref(pm, :region, current_region, "weight_b2b")

    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )
            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, gen["cost"][1]/A*reg_coef)
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, (gen["cost"][1]*pg + gen["cost"][2])/A*reg_coef)
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, (gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])/A*reg_coef)
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        collection = ref(pm, n, :region, current_region,"out_regionpairs_collection") 
        for (reg1, reg2, j) in collection
            vr_j = var(pm, n, :vr, j)
            vi_j = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, (reg1, reg2, j))
            betai = var(pm, n, :beta_i, (reg1, reg2, j))

            voltage_copy[(n,(reg1, reg2, j))] = JuMP.@expression(pm.model, 
                ρ/2*( (betar - vr_j)^2 + (betai - vi_j)^2 ) )
        end
    end

    return JuMP.@objective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_regionpairs_collection") )
        for (n, nw_ref) in nws(pm))
    )
end

""
function objective_min_fuel_cost_b2b_zero(pm::AbstractPowerModel; kwargs...)
    _objective_min_fuel_cost_b2b_zero(pm; kwargs...)
end
function _objective_min_fuel_cost_b2b_zero(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)

    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        collection = ref(pm, n, :region, current_region,"out_regionpairs_collection")
        for (reg1, reg2, j) in collection
            vm_j = var(pm, n, :vm, j)
            va_j = var(pm, n, :va, j)

            betar = var(pm, n, :beta_r, (reg1, reg2, j))
            betai = var(pm, n, :beta_i, (reg1, reg2, j))

            voltage_copy[(n,(reg1, reg2, j))] = JuMP.@NLexpression(pm.model, 
                ρ/2*( betar - vm_j*cos(va_j) )^2 +
                ρ/2*( betai - vm_j*sin(va_j) )^2
                )
        end
    end
    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_regionpairs_collection") )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_b2b_zero(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        collection = ref(pm, n, :region, current_region,"out_regionpairs_collection")
        for (reg1, reg2, j) in collection
            vr_j = var(pm, n, :vr, j)
            vi_j = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, (reg1, reg2, j))
            betai = var(pm, n, :beta_i, (reg1, reg2, j))

            voltage_copy[(n,(reg1, reg2, j))] = JuMP.@expression(pm.model, 
                ρ/2*( (betar-vr_j)^2 + (betai-vi_j)^2 ) )
        end
    end

    return JuMP.@objective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_regionpairs_collection") )
        for (n, nw_ref) in nws(pm))
    )
end











""
function objective_min_fuel_cost_r2r(pm::AbstractPowerModel; kwargs...)
    model = PowerModels.check_gen_cost_models(pm)

    if model == nothing
        return objective_min_fuel_cost_r2r_zero(pm; kwargs...)
    elseif model == 2
        return objective_min_fuel_cost_r2r_polynomial(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only cost models of types nothing and 2 are supported at this time, given cost model type of $(model)")
    end
end

""
function objective_min_fuel_cost_r2r_polynomial(pm::AbstractPowerModel; kwargs...)
    order = PowerModels.calc_max_cost_index(pm.data)-1

    if order <= 2
        return _objective_min_fuel_cost_r2r_polynomial_linquad(pm; kwargs...)
    else
        Memento.error(_LOGGER, "Only linear or quadratic models are supported at this time, given cost model order $(model)")
    end
end

""
function _objective_min_fuel_cost_r2r_polynomial_linquad(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    A = ref(pm, :norm_coef)
    reg_coef = ref(pm, :region, current_region, "weight_b2b")

    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )
            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, gen["cost"][1]/A*reg_coef)
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, (gen["cost"][1]*pg + gen["cost"][2])/A*reg_coef)
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, (gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])/A*reg_coef)
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@NLexpression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        collection = ref(pm, n, :region, current_region,"out_buslinks_collection")
        for (idlink, reg, j) in collection
            vm_j = var(pm, n, :vm, j)
            va_j = var(pm, n, :va, j)

            betar = var(pm, n, :beta_r, (idlink, reg, j))
            betai = var(pm, n, :beta_i, (idlink, reg, j))

            voltage_copy[(n,(idlink, reg, j))] = JuMP.@NLexpression(pm.model, 
                ρ/2*( betar - vm_j*cos(va_j))^2 +
                ρ/2*( betai - vm_j*sin(va_j))^2
                )
        end
    end

    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_buslinks_collection") )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_r2r_polynomial_linquad(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    A = ref(pm, :norm_coef)
    reg_coef = ref(pm, :region, current_region, "weight_b2b")

    gen_cost = Dict()
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        pg_cost = var(pm, n)[:pg_cost] = Dict{Int,Any}()
        for (i,gen) in nw_ref[:gen]
            pg = sum( var(pm, n, :pg, i)[c] for c in PowerModels.conductor_ids(pm, n) )
            if length(gen["cost"]) == 1
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, gen["cost"][1]/A*reg_coef)
            elseif length(gen["cost"]) == 2
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, (gen["cost"][1]*pg + gen["cost"][2])/A*reg_coef)
            elseif length(gen["cost"]) == 3
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, (gen["cost"][1]*pg^2 + gen["cost"][2]*pg + gen["cost"][3])/A*reg_coef)
            else
                gen_cost[(n,i)] = pg_cost[i] = JuMP.@expression(pm.model, 0.0)
            end
        end
        report && _IM.sol_component_value(pm, pm_it_sym, n, :gen, :pg_cost, ids(pm, n, :gen), pg_cost)

        collection = ref(pm, n, :region, current_region,"out_buslinks_collection") 
        for (idlink, reg, j) in collection
            vr_j = var(pm, n, :vr, j)
            vi_j = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, (idlink, reg, j))
            betai = var(pm, n, :beta_i, (idlink, reg, j))

            voltage_copy[(n,(idlink, reg, j))] = JuMP.@expression(pm.model, 
                ρ/2*( (betar - vr_j)^2 + (betai - vi_j)^2 ) )
        end
    end

    return JuMP.@objective(pm.model, Min,
        sum(
            sum( gen_cost[(n,i)] for (i,gen) in nw_ref[:gen] ) +
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_buslinks_collection") )
        for (n, nw_ref) in nws(pm))
    )
end

""
function objective_min_fuel_cost_r2r_zero(pm::AbstractPowerModel; kwargs...)
    _objective_min_fuel_cost_r2r_zero(pm; kwargs...)
end
function _objective_min_fuel_cost_r2r_zero(pm::ACPPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)

    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        collection = ref(pm, n, :region, current_region,"out_buslinks_collection")
        for (idlink, reg, j) in collection
            vm_j = var(pm, n, :vm, j)
            va_j = var(pm, n, :va, j)

            betar = var(pm, n, :beta_r, (idlink, reg, j))
            betai = var(pm, n, :beta_i, (idlink, reg, j))

            voltage_copy[(n,(idlink, reg, j))] = JuMP.@NLexpression(pm.model, 
                ρ/2*( betar - vm_j*cos(va_j) )^2 +
                ρ/2*( betai - vm_j*sin(va_j) )^2
                )
        end
    end
    return JuMP.@NLobjective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_buslinks_collection") )
        for (n, nw_ref) in nws(pm))
    )
end
function _objective_min_fuel_cost_r2r_zero(pm::ACRPowerModel; report::Bool=true)
    current_region = collect(ids(pm, :region))[1]
    ρ = ref(pm, :rho)
    
    voltage_copy = Dict()
    for (n, nw_ref) in nws(pm)
        collection = ref(pm, n, :region, current_region,"out_buslinks_collection")
        for (idlink, reg, j) in collection
            vr_j = var(pm, n, :vr, j)
            vi_j = var(pm, n, :vi, j)

            betar = var(pm, n, :beta_r, (idlink, reg, j))
            betai = var(pm, n, :beta_i, (idlink, reg, j))

            voltage_copy[(n,(idlink, reg, j))] = JuMP.@expression(pm.model, 
                ρ/2*( (betar-vr_j)^2 + (betai-vi_j)^2 ) )
        end
    end

    return JuMP.@objective(pm.model, Min,
        sum(
            sum( voltage_copy[(n,id)] for id in ref(pm, n, :region, current_region,"out_buslinks_collection") )
        for (n, nw_ref) in nws(pm))
    )
end
