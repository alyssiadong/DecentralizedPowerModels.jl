"
Adds the region dictionary and bus region component look-up.
Used on entire, non decentralized network representation.
Used in Erseghe and B2B and R2R.
"
function ref_add_regions!(ref::Dict{Symbol, <:Any}, data::Dict{String, <:Any})
    nw_ref = ref[:it][pm_it_sym][:nw][nw_id_default]
    n_regions = unique([bus["zone"] for (j,bus) in nw_ref[:bus]])
    regions = Dict{Int, Any}()
    for n_reg in n_regions
        region = Dict{Symbol, Any}()
        region[:bus] = [j for (j,bus) in nw_ref[:bus] if bus["zone"]==n_reg]
        region[:neigh_bus] = unique(
            [vcat([[j for (l,i0,j) in nw_ref[:bus_arcs][i]] for i in region[:bus]]...);
            region[:bus]])
        regions[n_reg] = region

        weight = 1.0
        weight_b2b = 1.0
        loc_x = loc_y = 0.5
        for (idx, data_region) in data["region"]
            if haskey(data_region, "region") && haskey(data_region, "loc_x") && haskey(data_region, "weight") && (data_region["region"] == n_reg)
                loc_x = data_region["loc_x"]
                loc_y = data_region["loc_y"]
                weight = data_region["weight"]
                weight_b2b = data_region["weightb2b"]
                break
            elseif haskey(data_region, "region") && haskey(data_region, "weight")  && (data_region["region"] == n_reg)
                weight = data_region["weight"]
                weight_b2b = data_region["weightb2b"]
                break
            elseif haskey(data_region, "region") && haskey(data_region, "loc_x") && (data_region["region"] == n_reg)
                loc_x = data_region["loc_x"]
                loc_y = data_region["loc_y"]
                break
            end            
        end
        region[:weight] = weight
        region[:weight_b2b] = weight_b2b
        region[:loc_x] = loc_x
        region[:loc_y] = loc_y
    end
    for (i, bus) in data["bus"]
        i_idx = parse(Int, i)
        nw_ref[:bus][i_idx]["duplicated_regions"] = [n_reg for (n_reg, reg) in regions if in(i_idx, reg[:neigh_bus])]
    end
    for n_reg in n_regions
        regions[n_reg][:outer_neigh_bus] = unique([j for j in regions[n_reg][:neigh_bus] if
                                                    Set(nw_ref[:bus][j]["duplicated_regions"]) != Set(n_reg) ])
        regions[n_reg][:arcs] = vcat([ [ arc for arc in nw_ref[:bus_arcs][j]] for j in regions[n_reg][:bus] ]...)

        regions[n_reg][:out_arcs] = [ (l,i,j)   for (l,i,j) in regions[n_reg][:arcs] if j∉regions[n_reg][:bus]]
        regions[n_reg][:in_arcs]  = [ (l,i,j)   for (l,i,j) in regions[n_reg][:arcs] if j∈regions[n_reg][:bus]]

        regions[n_reg][:branch] = unique([ l    for (l,i,j) in regions[n_reg][:arcs]])

        regions[n_reg][:arcs_from]      = [ arc for arc in regions[n_reg][:arcs]        if arc∈nw_ref[:arcs_from]]
        regions[n_reg][:out_arcs_from]  = [ arc for arc in regions[n_reg][:out_arcs]    if arc∈regions[n_reg][:arcs_from]]
        regions[n_reg][:in_arcs_from]   = [ arc for arc in regions[n_reg][:in_arcs]     if arc∈regions[n_reg][:arcs_from]]

        regions[n_reg][:arcs_to]        = [ arc for arc in regions[n_reg][:arcs]        if arc∈nw_ref[:arcs_to]]
        regions[n_reg][:out_arcs_to]    = [ arc for arc in regions[n_reg][:out_arcs]    if arc∈regions[n_reg][:arcs_to]]
        regions[n_reg][:in_arcs_to]     = [ arc for arc in regions[n_reg][:in_arcs]     if arc∈regions[n_reg][:arcs_to]]

        temp_out_buspairs               = [ buspair for (buspair, data) in nw_ref[:buspairs]
                                                if  ((buspair[1]∈regions[n_reg][:bus]) && (buspair[2]∉regions[n_reg][:bus])) || 
                                                    ((buspair[1]∉regions[n_reg][:bus]) && (buspair[2]∈regions[n_reg][:bus])) ]
        regions[n_reg][:out_buspairs]   = [ if (i∈regions[n_reg][:bus]) i,j else (j,i) end for (i,j)∈temp_out_buspairs ]
        regions[n_reg][:in_buspairs]    = [ buspair for (buspair, data) in nw_ref[:buspairs]
                                                if  ((buspair[1]∈regions[n_reg][:bus]) && (buspair[2]∈regions[n_reg][:bus])) ]

        out_buspairs_collection_dir = Dict(     (i,j,1) => Dict( 
                                                            "buspair" => (i,j),
                                                            "connected_region" => nw_ref[:bus][j]["zone"],
                                                            "bus" => i,
                                                            )
                                            for (i,j) in regions[n_reg][:out_buspairs]
                                            )
        out_buspairs_collection_rev = Dict(     (i,j,2) => Dict( 
                                                            "buspair" => (i,j),
                                                            "connected_region" => nw_ref[:bus][j]["zone"],
                                                            "bus" => j,
                                                            )
                                            for (i,j) in regions[n_reg][:out_buspairs]
                                            )
        regions[n_reg][:out_buspairs_collection] = merge(out_buspairs_collection_dir, out_buspairs_collection_rev)

        regions[n_reg][:duplicated_regions_local] = Dict{Int,Any}()
        for i_bus in regions[n_reg][:neigh_bus]
            if i_bus ∈ regions[n_reg][:bus]
                regions[n_reg][:duplicated_regions_local][i_bus] = nw_ref[:bus][i_bus]["duplicated_regions"]
            else
                regions[n_reg][:duplicated_regions_local][i_bus] = [n_reg, nw_ref[:bus][i_bus]["zone"]]
            end
        end
        regions[n_reg][:out_regionpairs_collection] = vcat([ 
                    [(n_reg, reg, j) for reg in dup_regions if reg ≠ n_reg] 
                    for (j, dup_regions) in regions[n_reg][:duplicated_regions_local] ]...)
        regions[n_reg][:out_regionlinks] = unique([ (reg1, reg2) for (reg1, reg2, bus) in regions[n_reg][:out_regionpairs_collection] ])
    end

    # Assigning unique ID to each regionlink
    regionlinks = unique(vcat([Set.(regions[n_reg][:out_regionlinks]) for n_reg in n_regions]...))
    id_regionlinks_temp = Dict(id => regionlinks[id] for id in 1:length(regionlinks))
    id_regionlinks = Dict{Int, Any}()
    for (id, link) in id_regionlinks_temp
        dic_temp = Dict{String, Any}()
        collected_link = collect(link)
        dic_temp["regionlink"] = link
        dic_temp["regionlink_eq"] = [collected_link, [collected_link[2], collected_link[1]]]
        id_regionlinks[id] = dic_temp
    end

    # Collecting regionlinks in regions dict
    for n_reg in n_regions
        dic_temp = Dict{Int, Any}()
        for (id, data) in id_regionlinks
            link_eq = data["regionlink_eq"]
            !((Tuple(link_eq[1]) ∈ regions[n_reg][:out_regionlinks]) || (Tuple(link_eq[2]) ∈ regions[n_reg][:out_regionlinks])) && continue
                reg2 = setdiff(collect(data["regionlink"]), n_reg)[1]
                dic_temp[id] = deepcopy(data)
                dic_temp[id]["neigh_region"] = reg2
                dic_temp[id]["duplicated_bus"] = [ j  for (k1, k2, j) in regions[n_reg][:out_regionpairs_collection] if ((k1==n_reg) && (k2==reg2))]
                dic_temp[id]["inside_bus"] = [ j for j in dic_temp[id]["duplicated_bus"] if j ∈ regions[n_reg][:bus]]
        end
        regions[n_reg][:out_regionlinks_collection] = dic_temp
        regions[n_reg][:out_buslinks_collection] = vcat([[(idlink, n_reg, j) for j in data["duplicated_bus"]] for (idlink, data) in regions[n_reg][:out_regionlinks_collection]]...)
    end
    nw_ref[:regionlinks] = id_regionlinks
    nw_ref[:region] = regions
end

"
Adds 'aj,h,k' and 'dj,k' coefficients.
Used on entire, non decentralized network representation.
Used in Erseghe.
"
function ref_add_admm_params!(ref::Dict{Symbol, <:Any}, data::Dict{String, <:Any})

    nw_ref = ref[:it][pm_it_sym][:nw][nw_id_default]
    buses_ref = nw_ref[:bus]
    ϵ = data["epsilon"]
    for (n_bus, bus_ref) in buses_ref
        regions = bus_ref["duplicated_regions"]
        Mj = length(regions)

        bus_ref["param_a"] = Dict{Tuple,Any}()
        bus_ref["param_a_tilde"] = Dict{Tuple,Any}()
        bus_ref["param_d"] = Dict{Int, Any}()

        for reg1 in regions
            for reg2 in regions
                if reg1 == reg2
                    a = 0
                else
                    weight1 = nw_ref[:region][reg1][:weight]
                    weight2 = nw_ref[:region][reg2][:weight]
                    a = (Mj == 1) ? 0 : ϵ*weight1*weight2/(Mj-1)
                    @assert (a≥0) "Parameter a must be positive."
                end
                bus_ref["param_a"][(reg1,reg2)] = a
            end
            d = sum([bus_ref["param_a"][(reg1, z)]  for z in regions])
            bus_ref["param_d"][reg1] = d
            for reg2 in regions
                if reg1 == reg2
                    at = 0
                else
                    @assert d≠0 "Divison by zero :("
                    at = bus_ref["param_a"][(reg1,reg2)]/d
                end
                bus_ref["param_a_tilde"][(reg1,reg2)] = at
            end
        end
    end
end

"
Adds starting value for voltage
Used on entire, non decentralized network representation.
Used in Erseghe and B2B.
"
function ref_add_starting_voltage_value!(ref::Dict{Symbol, <:Any}, data::Dict{String, <:Any})
    nw_ref = ref[:it][pm_it_sym][:nw][nw_id_default]
    for (i, bus) in data["bus"]
        i_idx = parse(Int, i)
        nw_ref[:bus][i_idx]["vm_start"] = bus["vm"]
        nw_ref[:bus][i_idx]["va_start"] = bus["va"]
        nw_ref[:bus][i_idx]["vr_start"] = bus["vm"]*cos(bus["va"])
        nw_ref[:bus][i_idx]["vi_start"] = bus["vm"]*sin(bus["va"])
    end
end

"
Adds starting value for generator power
Used on entire, non decentralized network representation.
Used in Erseghe and B2B.
"
function ref_add_starting_genpower_value!(ref::Dict{Symbol, <:Any}, data::Dict{String, <:Any})
    nw_ref = ref[:it][pm_it_sym][:nw][nw_id_default]
    for (i, gen) in data["gen"]
        i_idx = parse(Int, i)
        nw_ref[:gen][i_idx]["pg_start"] = gen["pg"]
        nw_ref[:gen][i_idx]["qg_start"] = gen["qg"]
    end
end