function admm_iteration(ds::DecentralizedSystem, solution_processors, optimizer=nothing)
    results = update_local_solution!(ds, optimizer, solution_processors)
    monitor_values!(ds, results)
    update_voltage_copy_variables!(ds)
    update_dual_variables!(ds)
    update_objective_functions!(ds)
    update_residuals!(ds)
    return results
end
function first_admm_iteration(ds::DecentralizedSystem, solution_processors, optimizer)
    results = update_local_solution!(ds, optimizer, solution_processors) # first time run to set optimizer
    monitor_values!(ds, results)
    initialize_voltage_copy_variables!(ds)
    initialize_dual_variables!(ds)
    update_objective_functions!(ds)
    return
end


function update_local_solution!(ds::DecentralizedSystem, optimizer, solution_processors)
    results = Dict{Int, Any}()
    for (i, lpm) in ds.pms
        results[i] = update_local_solution!(lpm.pm, optimizer, solution_processors)
    end
    return results
end
function update_local_solution!(pm::AbstractPowerModel, optimizer, solution_processors)
    start_time = time()
    results = optimize_model!(pm, optimizer = optimizer; solution_processors=solution_processors)
    Memento.debug(_LOGGER, "pm model solve and solution time: $(time() - start_time)")
    return results
end


function update_dual_variables!(ds::DecentralizedSystem)
    for (i, lpm) in ds.pms
        update_dual_variables!(lpm)
    end
end
function initialize_dual_variables!(ds::DecentralizedSystem)
    for (i, lpm) in ds.pms
        initialize_dual_variables!(lpm)
    end
end


function update_objective_functions!(ds::DecentralizedSystem)
    for (k, lpm) in ds.pms
        update_objective_functions!(lpm)
    end
end

function set_warm_start(ds::DecentralizedSystem, warm_start::Dict)
    for (k, lpm) in ds.pms
        set_warm_start(lpm, warm_start[k]["solution"])
    end
end
function set_warm_start(lpm::LocalPowerModel, warm_start::Dict)
    if length(warm_start)==0
        return 
    end
    for (nbus, bus) in PowerModels.ref(lpm.pm, :bus)
        if haskey(warm_start["bus"], string(nbus))
            if haskey(warm_start["bus"][string(nbus)], "vm")
                bus["vm_start"] = warm_start["bus"][string(nbus)]["vm"]
                bus["va_start"] = warm_start["bus"][string(nbus)]["va"]
            elseif haskey(warm_start["bus"][string(nbus)], "vr")
                bus["vr_start"] = warm_start["bus"][string(nbus)]["vr"]
                bus["vi_start"] = warm_start["bus"][string(nbus)]["vi"]
            else 
                Memento.error(_LOGGER, "Incorrect warm start dictionary.")
            end
        end
    end
end