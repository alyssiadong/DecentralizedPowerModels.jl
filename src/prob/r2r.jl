abstract type R2RType <: ProblemType end

"
Local power model
Allows complete code decentralization
"
mutable struct R2RLPM <: LocalPowerModel{R2RType}
    pm::AbstractPowerModel
    region::Int
    copyVariables::Dict{String,Any}
    dualVariables::Dict{String,Any}
    iteration::Int
    primal_res::Number
    dual_res::Number

    monitoring::Bool
    monitor::Dict
    monitor_time::Int
    monitor_max_time::Int
    comparison_optimal_res::Bool

    function R2RLPM(pm::AbstractPowerModel, monitoring=false::Bool, monitor_max_time=400::Int, comparison_optimal_res=false)
        @assert length(ids(pm, :region))==1  "Only one region per model"
        @assert monitor_max_time>0  "Monitor max time must be a positive integer"

        nregion = collect(ids(pm, :region))[1]
        collection = vcat(
                            vcat( [[[(idlink, nregion, j), (idlink, datalink["neigh_region"], j)] 
                                for j in datalink["duplicated_bus"]] 
                            for (idlink,datalink) in ref(pm, :region, nregion, "out_regionlinks_collection")]...)...)
        collection_dual =   vcat( [[(idlink, nregion, j) 
                                for j in datalink["duplicated_bus"]] 
                            for (idlink,datalink) in ref(pm, :region, nregion, "out_regionlinks_collection")]...)

        copyVariables = Dict(
            "voltage"        => Dict(idx => Complex(1.0) for idx in collection),
            "voltage_prev"   => Dict(idx => Complex(1.0) for idx in collection),
            )
        dualVariables = Dict(
            "lambda"         => Dict(idx => Complex(0.0) for idx in collection_dual)
            )
        iteration = 0

        if monitoring
            monitor = _initialize_local_monitor(pm, monitor_max_time)
        else
            monitor = Dict()
        end

        new(pm, nregion, copyVariables, dualVariables, 
            iteration, 0.0, 0.0, monitoring, monitor, 0, monitor_max_time, comparison_optimal_res)
    end
end

voltage_copy(lpm::R2RLPM, idx) = lpm.copyVariables["voltage"][idx]
voltage_copy_previous(lpm::R2RLPM, idx) = lpm.copyVariables["voltage_prev"][idx]
lambda(lpm::R2RLPM, idx) = lpm.dualVariables["lambda"][idx]

"
Contains all the local power models for each region of the global network.
"
mutable struct R2RDS <: DecentralizedSystem{R2RType}
    pms::Dict{Int64, R2RLPM}
    primal_res::Number
    dual_res::Number

    function R2RDS(pms::Dict{Int64, R2RLPM})
        new(pms,0.0,0.0)
    end
end

function run_dec_opf_r2r(file, model_type::Type, optimizer; monitor_max_time=200::Int, solution_processors=[], kwargs...)
    start_time = time()
    ds = instantiate_model_dec_r2r(file, model_type, build_dec_opf_r2r;
        ref_extensions=[],
        monitor_max_time = monitor_max_time,
        kwargs...)
    Memento.debug(_LOGGER, "ds model build time: $(time() - start_time)")

    first_admm_iteration(ds, solution_processors, optimizer)
    for k in 1:monitor_max_time-1
        (k%10==0) && Memento.info(_LOGGER, "ADMM iteration $k")
        admm_iteration(ds, solution_processors)
    end
    dec_results = admm_iteration(ds, solution_processors)

    truncate_monitor_values!(ds)
    monitor = build_global_monitor(ds)
    result = build_global_solution(dec_results, ds)

    return (result,monitor)
    # return ds
end

"
Create a power model for each bus which contains data about
each bus and its vicinity.
"
function instantiate_model_dec_r2r(file, model_type::Type, optimizer; monitor_max_time = 200, 
    monitoring=true, optimal_result::Dict=Dict(), kwargs...)
    # Instantiate simple model a first time in order to get "ref"
    pm = PowerModels.instantiate_model(file, model_type, PowerModels.build_opf;
        ref_extensions=[ref_add_regions!, #])
        ref_add_starting_voltage_value!, ref_add_starting_genpower_value!])

    data = pm.data

    if length(optimal_result)==0
        comparison_optimal_res = false
        res_dic = optimal_result
    elseif haskey(optimal_result, "solution")
        comparison_optimal_res = true
        res_dic = optimal_result["solution"]
    else
        comparison_optimal_res = true
        res_dic = optimal_result
    end

    pms = Dict{Int64, R2RLPM}()
    for (nregion, region) in ref(pm, :region)
        # For each region, creates a nw
        nw = Dict{String,Any}()

        # Copy region data
        nw["region"] = Dict{String,Any}(string(nregion)=>Dict{String,Any}())
        for (k,v) in region
            nw["region"][string(nregion)][string(k)] = v
        end

        # Copy internal and external buses
        nw["bus"] = Dict{String,Any}()
        if haskey(res_dic, "bus")
            for j in region[:neigh_bus]
                nw["bus"][string(j)] = deepcopy(data["bus"][string(j)])
                nw["bus"][string(j)]["optimal"] = res_dic["bus"][string(j)]
            end
        else
            for j in region[:neigh_bus]
                nw["bus"][string(j)] = deepcopy(data["bus"][string(j)])
            end
        end

        # Copy branches
        nw["branch"] = Dict{String,Any}()
        if haskey(res_dic, "branch")
            for l in region[:branch]
                nw["branch"][string(l)] = deepcopy(data["branch"][string(l)])
                nw["branch"][string(l)]["optimal"] = res_dic["branch"][string(l)]
            end
        else
            for l in region[:branch]
                nw["branch"][string(l)] = deepcopy(data["branch"][string(l)])
            end
        end

        # Copy gens, loads, shunts and storages
        gens = vcat(    [   ref(pm,:bus_gens, nbus)    for nbus in region[:bus]]...)
        loads = vcat(   [   ref(pm,:bus_loads, nbus)   for nbus in region[:bus]]...)
        shunts = vcat(  [   ref(pm,:bus_shunts, nbus)  for nbus in region[:bus]]...)
        storages = vcat([   ref(pm,:bus_storage, nbus) for nbus in region[:bus]]...)

        nw["gen"] = Dict{String,Any}()
        if haskey(res_dic, "gen")
            for gen in gens
                nw["gen"][string(gen)] = deepcopy(data["gen"][string(gen)])
                nw["gen"][string(gen)]["optimal"] = res_dic["gen"][string(gen)]
            end
        else
            for gen in gens
                nw["gen"][string(gen)] = deepcopy(data["gen"][string(gen)])
            end
        end

        nw["load"] = Dict{String,Any}()
        for load in loads
            nw["load"][string(load)] = deepcopy(data["load"][string(load)])
        end

        nw["shunt"] = Dict{String,Any}()
        for shunt in shunts
            nw["shunt"][string(shunt)] = deepcopy(data["shunt"][string(shunt)])
        end

        nw["storage"] = Dict{String,Any}()
        for storage in storages
            nw["storage"][string(storage)] = deepcopy(data["storage"][string(storage)])
        end

        # etc
        nw["source_version"] = data["source_version"]
        nw["source_type"] = data["source_type"]
        nw["name"] = string(data["name"],"_region", nregion)
        nw["baseMVA"] = data["baseMVA"]
        nw["dcline"] = Dict{String,Any}()
        nw["genpos"] = Dict{String,Any}()
        nw["switch"] = Dict{String,Any}()
        nw["multinetwork"] = false
        nw["per_unit"] = data["per_unit"]
        nw["rho"] = data["rho"]
        nw["norm_coef"] = data["norm_coef"]

        pm_new = instantiate_model(nw, model_type, build_dec_opf_r2r; kwargs...)
        pms[nregion] = R2RLPM(pm_new, monitoring, monitor_max_time, comparison_optimal_res)
    end

    R2RDS(pms)
end

function build_dec_opf_r2r(pm::AbstractPowerModel)
    region_idx = collect(ids(pm, :region))[1]    # region associated to the network

    PowerModels.variable_bus_voltage(pm)
    PowerModels.variable_gen_power(pm)
    variable_branch_power_dec(pm)

    PowerModels.constraint_model_voltage(pm)

    for i in ids(pm, :ref_buses)
        PowerModels.constraint_theta_ref(pm, i)
    end

    for i in ref(pm, :region, region_idx, "bus")
        PowerModels.constraint_power_balance(pm, i)
    end

    for idx in ref(pm, :region, region_idx, "arcs_from")
        l,i,j = idx
        PowerModels.constraint_ohms_yt_from(pm, l)
        PowerModels.constraint_thermal_limit_from(pm, l)
        PowerModels.constraint_voltage_angle_difference(pm, l)
    end
    for idx in ref(pm, :region, region_idx, "arcs_to")
        l,i,j = idx
        PowerModels.constraint_ohms_yt_to(pm, l)
        PowerModels.constraint_thermal_limit_to(pm, l)
        if idx in ref(pm,:region, region_idx, "out_arcs_to")
            PowerModels.constraint_voltage_angle_difference(pm,l)
        end
    end

    parameter_dual_variable_r2r(pm; report=false)
    expression_primal_residual_r2r(pm; report=true)
    expression_dual_residual_r2r(pm; report=true)
    expression_lambda_r2r(pm; report=true)
    objective_min_fuel_cost_r2r(pm; report=false)
    expression_objective_gencost(pm;report=true)
    expression_solution_comparison(pm;report=true)
end

function parameter_dual_variable_r2r(pm::AbstractPowerModel; nw::Int=nw_id_default, report::Bool=false)
    current_region = collect(ids(pm, nw, :region))[1]
    collection = ref(pm, nw, :region, current_region, "out_buslinks_collection")
    beta_r = var(pm,nw)[:beta_r] = JuMP.@variable(pm.model,
        beta_r[(idlink, reg, j) in collection] == 0.0 
        )
    beta_i = var(pm,nw)[:beta_i] = JuMP.@variable(pm.model,
        beta_i[(idlink, reg, j) in collection] == 0.0
        )

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :beta_r, collection, beta_r)
    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :beta_i, collection, beta_i)
end

function expression_primal_residual_r2r(pm::AbstractPowerModel; nw::Int=nw_id_default, report::Bool=true)
    # Setting primal res as a fixed variable for monitoring purposes
    current_region = collect(ids(pm, nw, :region))[1]
    collection = ref(pm, nw, :region, current_region,"out_buslinks_collection")
    primal_res = var(pm,nw)[:primal_res] = JuMP.@variable(pm.model,
        primal_res[(idlink, reg, j) in collection] == 0.0 
        )
    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :primal_res, collection, primal_res)
end
function expression_dual_residual_r2r(pm::AbstractPowerModel; nw::Int=nw_id_default, report::Bool=true)
    # Setting dual res as a fixed variable for monitoring purposes
    current_region = collect(ids(pm, nw, :region))[1]
    collection = ref(pm, nw, :region, current_region,"out_buslinks_collection")
    dual_res = var(pm,nw)[:dual_res] = JuMP.@variable(pm.model,
        dual_res[(idlink, reg, j) in collection] == 0.0 
        )
    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :dual_res, collection, dual_res)
end

function expression_lambda_r2r(pm::AbstractPowerModel; nw::Int=nw_id_default, report::Bool=false)
    current_region = collect(ids(pm, nw, :region))[1]
    collection = ref(pm, nw, :region, current_region,"out_buslinks_collection")
    lambda_r = var(pm,nw)[:lambda_r] = JuMP.@variable(pm.model,
        lambda_r[(idlink, reg, j) in collection] == 0.0 
        )
    lambda_i = var(pm,nw)[:lambda_i] = JuMP.@variable(pm.model,
        lambda_i[(idlink, reg, j) in collection] == 0.0
        )

    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :lambda_r, collection, lambda_r)
    report && _IM.sol_component_value(pm, pm_it_sym, nw, :regionpairs, :lambda_i, collection, lambda_i)
end

### expression_objective_gencost function in b2b.jl

"
ADMM iteration
"
function update_voltage_copy_variables!(ds::R2RDS)
    for (k, lpm) in ds.pms
        for (idlink, data) in ref(lpm, :region, k, "out_regionlinks_collection")
            neigh_region = data["neigh_region"]
            for j in data["duplicated_bus"]
                v = _read_complex_voltage(lpm, j)
                lpm.copyVariables["voltage_prev"][(idlink, k, j)] = lpm.copyVariables["voltage"][(idlink, k, j)]
                lpm.copyVariables["voltage"][(idlink, k, j)] = v

                # Neighbor region
                lpm_h = ds.pms[neigh_region]
                lpm_h.copyVariables["voltage_prev"][(idlink, k, j)] = lpm_h.copyVariables["voltage"][(idlink, k, j)]
                lpm_h.copyVariables["voltage"][(idlink, k, j)] = v
            end
        end
    end
end
function initialize_voltage_copy_variables!(ds::R2RDS)
    update_voltage_copy_variables!(ds)
end




function update_dual_variables!(lpm::R2RLPM)
    for nregion in ids(lpm, :region)                # only one region per pm
        for (idlink, reg, bus) in ref(lpm, :region, nregion, "out_buslinks_collection")
            update_dual_variables!(lpm, idlink, reg, bus)
        end
    end
end
update_dual_variables!(lpm::R2RLPM, idx::Tuple) = update_dual_variables!(lpm, idx[1], idx[2], idx[3])
function update_dual_variables!(lpm::R2RLPM, idlink::Int, reg::Int, bus::Int)
    nregion = lpm.region
    ρ = ref(lpm, :rho)

    data = ref(lpm, :region, nregion, "out_regionlinks_collection")[idlink]
    neigh_region = data["neigh_region"]

    idx_local = (idlink,reg,bus)
    idx = (idlink,neigh_region,bus)
    lpm.dualVariables["lambda"][idx_local] -= ρ/2*(voltage_copy(lpm,idx_local)-voltage_copy(lpm,idx))
    _set_lambda(lpm, idx_local, lambda(lpm, idx_local))
end
function initialize_dual_variables!(lpm::R2RLPM)
    update_dual_variables!(lpm)
end

_set_lambda(lpm::R2RLPM, idx, value::Complex) = _set_lambda(lpm.pm, idx, value)
### _set_lambda in b2b.jl




function update_objective_functions!(lpm::R2RLPM)
    nregion = lpm.region
    for (idlink, reg, bus) in ref(lpm,:region, nregion, "out_buslinks_collection")
        update_objective_functions!(lpm, idlink, reg, bus)
    end
end
update_objective_functions!(lpm::R2RLPM, idx::Tuple) = update_objective_functions!(lpm, idx[1], idx[2], idx[3])
function update_objective_functions!(lpm::R2RLPM, idlink::Int, reg::Int, bus::Int)
    nregion = lpm.region
    ρ = ref(lpm, :rho)

    data = ref(lpm, :region, nregion, "out_regionlinks_collection")[idlink]
    neigh_region = data["neigh_region"]

    idx_local = (idlink,reg,bus)
    idx = (idlink,neigh_region,bus)

    beta_value = (voltage_copy(lpm,idx_local) + voltage_copy(lpm,idx))/2 + lambda(lpm,idx_local)/ρ
    _set_beta(lpm, idx_local, beta_value)
end

_set_beta(lpm::R2RLPM, idx, beta_value::Complex) = _set_beta(lpm.pm, idx, beta_value)
### _set_beta in b2b.jl





function update_residuals!(ds::R2RDS)
    for (i, lpm) in ds.pms
        update_residuals!(lpm)
    end
    ds.primal_res = compute_global_primal_residual(ds)
    ds.dual_res = compute_global_dual_residual(ds)
end
function update_residuals!(lpm::R2RLPM)
    terms_prim = Dict()
    terms_dual = Dict()
    for (nregion, region) in ref(lpm,:region)            # only one region per pm
        for (idlink, reg, bus) in ref(lpm, :region, nregion, "out_buslinks_collection")
            data = ref(lpm, :region, nregion, "out_regionlinks_collection")[idlink]
            neigh_region = data["neigh_region"]

            idx_local = (idlink, reg, bus)
            idx = (idlink,neigh_region,bus)

            prim = norm(voltage_copy(lpm,idx_local)-voltage_copy(lpm,idx))^2
            dual = norm(voltage_copy(lpm,idx_local)-voltage_copy_previous(lpm,idx_local))^2

            terms_prim[idx] = prim
            terms_dual[idx] = dual

            _set_primal_res(lpm.pm,idx_local,prim)
            _set_dual_res(lpm.pm,idx_local,dual)
        end
    end
    lpm.primal_res = sum(x for (k,x) in terms_prim)
    lpm.dual_res = sum(x for (k,x) in terms_dual)
end
### _set_primal_res in b2b.jl
### _set_dual_res in b2b.jl
compute_global_primal_residual(ds::R2RDS)   = sum(lpm.primal_res for (i,lpm) in ds.pms)
compute_global_dual_residual(ds::R2RDS)     = sum(lpm.dual_res for (i,lpm) in ds.pms)




"
Solution building, creating one dictionary from the monitor of the decentralized ones.
"
function build_global_monitor(ds::R2RDS)
    result = Dict(  "bus"=>         Dict{String,Any}(),
                    "branch"=>      Dict{String,Any}(),
                    "gen"=>         Dict{String,Any}(),
                    "region"=>      Dict{String,Any}(),
                    "regionpairs"=>    Dict{String,Any}(),
                    "objective"=>   Float64[],
                    )
    for (i,lpm) in ds.pms
        if lpm.monitoring
            region = lpm.region
            build_global_monitor_r2r(i, result, lpm.monitor, ref(lpm, :region, region, "bus"))
        end
    end

    if all([lpm.monitoring for (i,lpm) in ds.pms])
        result["objective"] = sum( gen["pg_cost"] for (i, gen) in result["gen"] )
    end

    return result
end
function build_global_monitor_r2r(i::Int, result::Dict{String,<:Any}, 
    local_monitor::Dict{String,<:Any}, local_buses::AbstractVector)

    result["region"][string(i)] = Dict{String,Any}()
    result["region"][string(i)]["primal_res"]   = _regionwise_primal_residual(local_monitor)
    result["region"][string(i)]["dual_res"]     = _regionwise_dual_residual(local_monitor)
    if haskey(local_monitor["bus"], "0")
        result["region"][string(i)]["voltage_comp"] = local_monitor["bus"]["0"]["comp_optimal"]
        result["region"][string(i)]["branch_comp"] = local_monitor["branch"]["0"]["comp_optimal"]
        result["region"][string(i)]["gen_comp"] = (haskey(local_monitor, "gen")) ? local_monitor["gen"]["0"]["comp_optimal"] : 0
    end

    for (key,dic) in local_monitor
        if key=="bus"
            for bus in local_buses
                result[key][string(bus)] = deepcopy(dic[string(bus)])
            end
        elseif (key == "iteration_counter") || (key == "local_iteration")
            result["region"][string(i)][key] = deepcopy(dic)
        else
            for (idx,vals) in dic
                if idx != "0"
                    if !haskey(result[key], idx)
                        result[key][idx] = deepcopy(vals)
                    else
                        for (k,v) in vals
                            result[key][idx][k] = v
                        end
                    end
                end
            end
        end
    end
end
### _regionwise_primal_residual in b2b.jl
### _regionwise_dual_residual in b2b.jl


"
"
### _read_complex_voltage in b2b.jl
### _read_complex_voltage_init in b2b.jl
