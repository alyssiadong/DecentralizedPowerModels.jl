"
Local monitor
"
function _initialize_local_monitor(pm::AbstractPowerModel, monitor_max_time::Int)
    # Get its form from the sol dictionary
    # Doesn't work with multinetwork
    return _initialize_local_monitor(pm.sol[:it][pm_it_sym][:nw][nw_id_default], monitor_max_time)
end
function _initialize_local_monitor(var::Dict, monitor_max_time::Int)
    out = Dict{String,Any}()
    for (key, val) in var
        out[string(key)] = _initialize_local_monitor(val, monitor_max_time)
    end
    return out
end
function _initialize_local_monitor(var, monitor_max_time::Int)
    return zeros(monitor_max_time)
end


""
function monitor_values!(ds::DecentralizedSystem, results::Dict{Int,<:Any})
    for (i,lpm) in ds.pms
        monitor_values!(lpm, results[i])
    end
end
function monitor_values!(lpm::LocalPowerModel, results::Dict{String,<:Any})
    if (!lpm.monitoring) || (lpm.monitor_max_time ≤ lpm.monitor_time)
        return
    else
        lpm.monitor_time+=1
        monitor_values!(lpm.monitor, results["solution"], lpm.monitor_time)
    end
end
function monitor_values!(data::Dict, results::Dict, monitor_time::Int)
    if _IM._iscomponentdict(data)
        for (k,item) in data
            monitor_values!(item, results[k], monitor_time)
        end
    else
        for (k,timeseries) in data
            data[k][monitor_time] = results[k]
        end
    end
end

""
function truncate_monitor_values!(ds::DecentralizedSystem)
    for (i,lpm) in ds.pms
        truncate_monitor_values!(lpm)
    end
end
function truncate_monitor_values!(lpm::LocalPowerModel)
    if (!lpm.monitoring)
        return
    else
        truncate_monitor_values!(lpm.monitor, lpm.monitor_time)
    end
end
function truncate_monitor_values!(data::Dict, monitor_time::Int)
    if _IM._iscomponentdict(data)
        for (k,item) in data
            truncate_monitor_values!(item, monitor_time)
        end
    else
        for (k,timeseries) in data
            data[k] = data[k][1:monitor_time]
        end
    end
end
