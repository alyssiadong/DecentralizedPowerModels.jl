abstract type ProblemType end

abstract type LocalPowerModel{T<:ProblemType} end
ids(lpm::LocalPowerModel, key::Symbol) = ids(lpm.pm, key)

ref(lpm::LocalPowerModel) = ref(lpm.pm)
ref(lpm::LocalPowerModel, key::Symbol) = ref(lpm.pm, key)
ref(lpm::LocalPowerModel, key::Symbol, idx) = ref(lpm.pm, key, idx)
ref(lpm::LocalPowerModel, key::Symbol, idx, param::String) = ref(lpm.pm, key, idx, param)

var(lpm::LocalPowerModel) = var(lpm.pm)
var(lpm::LocalPowerModel, key::Symbol) = var(lpm.pm, key)
var(lpm::LocalPowerModel, key::Symbol, idx) = var(lpm.pm, key, idx)

sol(lpm::LocalPowerModel, args...) = sol(lpm.pm, args...)

"
Decentralized system
Contains all the local power models for each region of the global network.
"

abstract type DecentralizedSystem{T<:ProblemType} end
ids(ds::DecentralizedSystem, k::Int, key::Symbol) = ids(ds.pms[k], key)

ref(ds::DecentralizedSystem, k::Int) = ref(ds.pms[k])
ref(ds::DecentralizedSystem, k::Int, key::Symbol) = ref(ds.pms[k], key)
ref(ds::DecentralizedSystem, k::Int, key::Symbol, idx) = ref(ds.pms[k], key, idx)
ref(ds::DecentralizedSystem, k::Int, key::Symbol, idx, param::String) = ref(ds.pms[k], key, idx, param)

var(ds::DecentralizedSystem, k::Int) = var(ds.pms[k])
var(ds::DecentralizedSystem, k::Int, key::Symbol) = var(ds.pms[k], key)
var(ds::DecentralizedSystem, k::Int, key::Symbol, idx) = var(ds.pms[k], key, idx)

sol(ds::DecentralizedSystem, k::Int, args...) = sol(ds.pms[k], args...)


"
Build global solution from local solution dictionaries
"
function build_global_solution(dec_results::Dict, ds::DecentralizedSystem)
    result = Dict{String, Any}()
    result["bus"] = Dict{String, Any}()
    result["branch"] = Dict{String, Any}()
    result["gen"] = Dict{String, Any}()
    for (i,dec_res) in dec_results
        region_buses = ref(ds, i, :region, i)["bus"]
        for bus in region_buses
            result["bus"][string(bus)] = dec_res["solution"]["bus"][string(bus)]
            delete!(result["bus"][string(bus)], "vanish")
        end
        _copy_solutions(dec_res, result)
    end
    result["baseMVA"] = dec_results[collect(keys(dec_results))[1]]["solution"]["baseMVA"]
    result["per_unit"] = dec_results[collect(keys(dec_results))[1]]["solution"]["per_unit"]

    result["termination_status"] = Dict(i=>dec_res["termination_status"] for (i, dec_res) in dec_results)
    result["objective"] = sum(
        sum(Float64[JuMP.value(var) for (id, var) in var(lpm, :pg_cost)])
        for (i,lpm) in ds.pms if haskey(var(lpm), :pg_cost))

    return result
end
function _copy_solutions(dec_res::Dict{String,Any}, res::Dict{String, Any})
    keywords = ["branch", "gen"]
    for key in keywords
        if haskey(dec_res["solution"], key)
            for (i, var) in dec_res["solution"][key]
                if !haskey(res[key],i)
                    res[key][i] = var
                else
                    for (k,v) in var
                        res[key][i][k] = v
                    end
                end
            end
        end
    end
end

"
"
_read_complex_voltage(lpm::LocalPowerModel, i::Int) = _read_complex_voltage(lpm.pm, i)
function _read_complex_voltage(pm::ACPPowerModel, i::Int)
    va = JuMP.value(sol(pm, :bus, i)[:va])
    vm = JuMP.value(sol(pm, :bus, i)[:vm])

    v = vm*exp(im*va)
    return v
end
function _read_complex_voltage(pm::ACRPowerModel, i::Int)
    vr = JuMP.value(sol(pm, :bus, i)[:vr])
    vi = JuMP.value(sol(pm, :bus, i)[:vi])

    v = vr + im*vi
    return v
end

_read_complex_voltage_init(lpm::LocalPowerModel, i::Int) = _read_complex_voltage_init(lpm.pm, i)
function _read_complex_voltage_init(pm::ACPPowerModel, i::Int)
    nw = nw_id_default

    vm = comp_start_value(ref(pm, nw, :bus, i), "vm_start", 1.0)
    va = comp_start_value(ref(pm, nw, :bus, i), "va_start", 0.0)

    v = vm*exp(im*va)
    return v
end
function _read_complex_voltage_init(pm::ACRPowerModel, i::Int)
    nw = nw_id_default

    vr = comp_start_value(ref(pm, nw, :bus, i), "vr_start", 1.0)
    vi = comp_start_value(ref(pm, nw, :bus, i), "vi_start", 0.0)

    v = vr + im*vi
    return v
end