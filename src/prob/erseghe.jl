abstract type ErsegheType <: ProblemType end


"
Local power model
Allows complete code decentralization
"
mutable struct ErsegheLPM <: LocalPowerModel{ErsegheType}
    pm::AbstractPowerModel
    region::Int
    copyVariables::Dict{String,Any}
    dualVariables::Dict{String,Any}
    iteration::Int
    vanishingMeasure::Number
    vanishingMeasureMA::Number

    monitoring::Bool
    monitor::Dict
    monitor_time::Int
    monitor_max_time::Int
    comparison_optimal_res::Bool

    function ErsegheLPM(pm::AbstractPowerModel, monitoring=false::Bool, monitor_max_time=400::Int, comparison_optimal_res=false)
        @assert length(ids(pm, :region))==1  "Only one region per model"
        @assert monitor_max_time>0  "Monitor max time must be a positive integer"

        nregion = collect(ids(pm, :region))[1]
        neigh_bus = ref(pm, :region, nregion, "neigh_bus")
        copyVariables = Dict(
            "voltage"            => Dict(bus => Dict( region => Complex(1.0)
                                        for region in ref(pm, :bus, bus, "duplicated_regions"))
                                        for bus in neigh_bus),
            "voltage_prev"       => Dict(bus => Dict( region => Complex(1.0)
                                        for region in ref(pm, :bus, bus, "duplicated_regions"))
                                        for bus in neigh_bus),
            )
        dualVariables = Dict(
            "mixing_value"       => Dict(bus => Complex(0.0) for bus in neigh_bus),
            "mixing_value_prev"  => Dict(bus => Complex(0.0) for bus in neigh_bus),
            "memory"             => Dict(bus => Complex(0.0) for bus in neigh_bus),
            )
        iteration = 0
        vanishingMeasure = Inf
        vanishingMeasureMA = Inf

        if monitoring
            monitor = _initialize_local_monitor(pm, monitor_max_time)
        else
            monitor = Dict()
        end

        new(pm, nregion, copyVariables, dualVariables, 
            iteration, vanishingMeasure, vanishingMeasureMA, 
            monitoring, monitor, 0, monitor_max_time, comparison_optimal_res)
    end
    function ErsegheLPM(pm::AbstractPowerModel, warm_start::Dict, monitoring=false::Bool, monitor_max_time=400::Int, comparison_optimal_res=false)
        @assert length(ids(pm, :region))==1  "Only one region per model"
        @assert monitor_max_time>0  "Monitor max time must be a positive integer"

        nregion = collect(ids(pm, :region))[1]
        neigh_bus = ref(pm, :region, nregion, "neigh_bus")

        warm_start_local = warm_start[nregion]["solution"]

        haskeyvr = all([haskey(v, "vr") for (k,v) in warm_start_local["bus"]])
        haskeyvm = all([haskey(v, "vm") for (k,v) in warm_start_local["bus"]])
        haskeymemoryr = all([haskey(v, "memory_r") for (k,v) in warm_start_local["bus"]])
        haskeymemorym = all([haskey(v, "memory_m") for (k,v) in warm_start_local["bus"]])

        if haskeyvr
            copyVariables = Dict(
                "voltage"            => Dict(bus => Dict( region => warm_start_local["bus"][string(bus)]["vr"] + im*warm_start_local["bus"][string(bus)]["vi"]
                                            for region in ref(pm, :bus, bus, "duplicated_regions"))
                                            for bus in neigh_bus),
                "voltage_prev"       => Dict(bus => Dict( region => warm_start_local["bus"][string(bus)]["vr"] + im*warm_start_local["bus"][string(bus)]["vi"]
                                            for region in ref(pm, :bus, bus, "duplicated_regions"))
                                            for bus in neigh_bus),
                )
        elseif haskeyvm
            copyVariables = Dict(
                "voltage"            => Dict(bus => Dict( region => warm_start_local["bus"][string(bus)]["vm"]*exp(im*warm_start_local["bus"][string(bus)]["va"])
                                            for region in ref(pm, :bus, bus, "duplicated_regions"))
                                            for bus in neigh_bus),
                "voltage_prev"       => Dict(bus => Dict( region => warm_start_local["bus"][string(bus)]["vm"]*exp(im*warm_start_local["bus"][string(bus)]["va"])
                                            for region in ref(pm, :bus, bus, "duplicated_regions"))
                                            for bus in neigh_bus),
                )
        else
            Memento.error(_LOGGER, "Incorrect warm start dictionary.")
        end
        
        if haskeymemoryr
            dualVariables = Dict(
                "mixing_value"       => Dict(bus => Complex(0.0) for bus in neigh_bus),
                "mixing_value_prev"  => Dict(bus => Complex(0.0) for bus in neigh_bus),
                "memory"             => Dict(bus => warm_start_local["bus"][string(bus)]["memory_r"]+im*warm_start_local["bus"][string(bus)]["memory_i"] for bus in neigh_bus),
                )
        elseif haskeymemorym
            dualVariables = Dict(
                "mixing_value"       => Dict(bus => Complex(0.0) for bus in neigh_bus),
                "mixing_value_prev"  => Dict(bus => Complex(0.0) for bus in neigh_bus),
                "memory"             => Dict(bus => warm_start_local["bus"][string(bus)]["memory_m"]*exp(im*warm_start_local["bus"][string(bus)]["memory_a"]) for bus in neigh_bus),
                )
        else
            Memento.error(_LOGGER, "Incorrect warm start dictionary.")
        end
        iteration = 0
        vanishingMeasure = Inf
        vanishingMeasureMA = Inf

        if monitoring
            monitor = _initialize_local_monitor(pm, monitor_max_time)
        else
            monitor = Dict()
        end

        new(pm, nregion, copyVariables, dualVariables, 
            iteration, vanishingMeasure, vanishingMeasureMA,
            monitoring, monitor, 0, monitor_max_time, comparison_optimal_res)
    end
end

voltage_copy(lpm::ErsegheLPM, j::Int, k::Int=lpm.region) = lpm.copyVariables["voltage"][j][k]
voltage_copy_previous(lpm::ErsegheLPM, j::Int, k::Int=lpm.region) = lpm.copyVariables["voltage_prev"][j][k]

"
Contains all the local power models for each region of the global network.
"
mutable struct ErsegheDS <: DecentralizedSystem{ErsegheType}
    pms::Dict{Int64, ErsegheLPM}
    vanishing_measure::Number
    vanishing_measure_MA::Number

    function ErsegheDS(pms::Dict{Int64, ErsegheLPM})
        vanishing_measure = Inf
        vanishing_measure_MA = Inf
        new(pms, vanishing_measure, vanishing_measure_MA)
    end
end

voltage_copy(ds::ErsegheDS, j::Int, k::Int) = voltage_copy(ds.pms[k],j)
voltage_copy(ds::ErsegheDS, j::Int) = Dict(k=>voltage_copy(ds,j,k) for k in keys(ds.pms))
voltage_copy_previous(ds::ErsegheDS, j::Int, k::Int) = voltage_copy_previous(ds.pms[k],j)
voltage_copy_previous(ds::ErsegheDS, j::Int) = Dict(k=>voltage_copy_previous(ds,j,k) for k in keys(ds.pms))

function run_dec_opf_erseghe(file, model_type::Type, optimizer; 
    monitor_max_time=200::Int, solution_processors=[], warm_start::Dict=Dict(), kwargs...)
    
    start_time = time()
    ds = instantiate_model_dec_erseghe(file, model_type, build_dec_opf_erseghe;
        ref_extensions=[],
        monitor_max_time = monitor_max_time,
        warm_start = warm_start,
        kwargs...)
    Memento.debug(_LOGGER, "ds model build time: $(time() - start_time)")

    (length(warm_start)>0) && set_warm_start(ds, warm_start)
    first_admm_iteration(ds, solution_processors, optimizer)
    for k in 1:monitor_max_time-1
        # (k>20) && (abs(ds.vanishing_measure) < 1e-6) && break
        (k%50==0) && Memento.info(_LOGGER, "ADMM iteration $k")
        admm_iteration(ds, solution_processors)
    end
    dec_results = admm_iteration(ds, solution_processors)

    truncate_monitor_values!(ds)
    monitor = build_global_monitor(ds)
    result = build_global_solution(dec_results, ds)

    return (result,monitor, dec_results)
end

"
Create a power model for each bus which contains data about
each bus and its vicinity.
"
function instantiate_model_dec_erseghe(file, model_type::Type, optimizer; 
    monitor_max_time = 200, monitoring=true, optimal_result::Dict=Dict(), warm_start::Dict=Dict(), kwargs...)
    # Instantiate simple model a first time in order to get "ref"
    pm = PowerModels.instantiate_model(file, model_type, PowerModels.build_opf;
        ref_extensions=[ref_add_regions!, ref_add_admm_params!, #])
        ref_add_starting_voltage_value!, ref_add_starting_genpower_value!])

    data = pm.data

    if length(optimal_result)==0
        comparison_optimal_res = false
        res_dic = optimal_result
    elseif haskey(optimal_result, "solution")
        comparison_optimal_res = true
        res_dic = optimal_result["solution"]
    else
        comparison_optimal_res = true
        res_dic = optimal_result
    end

    pms = Dict{Int64, ErsegheLPM}()
    for (nregion, region) in ref(pm, :region)
        # For each region, creates a nw
        nw = Dict{String,Any}()

        # Copy region data
        nw["region"] = Dict{String,Any}(string(nregion)=>Dict{String,Any}())
        for (k,v) in region
            nw["region"][string(nregion)][string(k)] = v
        end

        # Copy internal and external buses
        nw["bus"] = Dict{String,Any}()
        if haskey(res_dic, "bus")
            for j in region[:neigh_bus]
                nw["bus"][string(j)] = deepcopy(data["bus"][string(j)])
                nw["bus"][string(j)]["optimal"] = res_dic["bus"][string(j)]
            end
        else
            for j in region[:neigh_bus]
                nw["bus"][string(j)] = deepcopy(data["bus"][string(j)])
            end
        end

        # Copy branches
        nw["branch"] = Dict{String,Any}()
        if haskey(res_dic, "branch")
            for l in region[:branch]
                nw["branch"][string(l)] = deepcopy(data["branch"][string(l)])
                nw["branch"][string(l)]["optimal"] = res_dic["branch"][string(l)]
            end
        else
            for l in region[:branch]
                nw["branch"][string(l)] = deepcopy(data["branch"][string(l)])
            end
        end

        # Copy gens, loads, shunts and storages
        gens = vcat(    [   ref(pm,:bus_gens, nbus)    for nbus in region[:bus]]...)
        loads = vcat(   [   ref(pm,:bus_loads, nbus)   for nbus in region[:bus]]...)
        shunts = vcat(  [   ref(pm,:bus_shunts, nbus)  for nbus in region[:bus]]...)
        storages = vcat([   ref(pm,:bus_storage, nbus) for nbus in region[:bus]]...)

        nw["gen"] = Dict{String,Any}()
        if haskey(res_dic, "gen")
            for gen in gens
                nw["gen"][string(gen)] = deepcopy(data["gen"][string(gen)])
                nw["gen"][string(gen)]["optimal"] = res_dic["gen"][string(gen)]
            end
        else
            for gen in gens
                nw["gen"][string(gen)] = deepcopy(data["gen"][string(gen)])
            end
        end

        nw["load"] = Dict{String,Any}()
        for load in loads
            nw["load"][string(load)] = deepcopy(data["load"][string(load)])
        end

        nw["shunt"] = Dict{String,Any}()
        for shunt in shunts
            nw["shunt"][string(shunt)] = deepcopy(data["shunt"][string(shunt)])
        end

        nw["storage"] = Dict{String,Any}()
        for storage in storages
            nw["storage"][string(storage)] = deepcopy(data["storage"][string(storage)])
        end

        # etc
        nw["source_version"] = data["source_version"]
        nw["source_type"] = data["source_type"]
        nw["name"] = string(data["name"],"_region", nregion)
        nw["baseMVA"] = data["baseMVA"]
        nw["dcline"] = Dict{String,Any}()
        nw["genpos"] = Dict{String,Any}()
        nw["switch"] = Dict{String,Any}()
        nw["multinetwork"] = false
        nw["per_unit"] = data["per_unit"]
        nw["epsilon"] = data["epsilon"]

        pm_new = instantiate_model(nw, model_type, build_dec_opf_erseghe; kwargs...)
        if length(warm_start) != 0
            pms[nregion] = ErsegheLPM(pm_new, warm_start, monitoring, monitor_max_time, comparison_optimal_res)
        else
            pms[nregion] = ErsegheLPM(pm_new, monitoring, monitor_max_time, comparison_optimal_res)
        end
    end

    ErsegheDS(pms)
end

function build_dec_opf_erseghe(pm::AbstractPowerModel)
    region_idx = collect(ids(pm, :region))[1]    # region associated to the network

    PowerModels.variable_bus_voltage(pm)
    PowerModels.variable_gen_power(pm)
    variable_branch_power_dec(pm)
    PowerModels.constraint_model_voltage(pm)

    for i in ids(pm, :ref_buses)
        PowerModels.constraint_theta_ref(pm, i)
    end

    for i in ref(pm, :region, region_idx, "bus")
        PowerModels.constraint_power_balance(pm, i)
    end

    for idx in ref(pm, :region, region_idx, "arcs_from")
        l,i,j = idx
        PowerModels.constraint_ohms_yt_from(pm, l)
        PowerModels.constraint_thermal_limit_from(pm, l)
        PowerModels.constraint_voltage_angle_difference(pm, l)
    end

    for idx in ref(pm, :region, region_idx, "arcs_to")
        l,i,j = idx
        PowerModels.constraint_ohms_yt_to(pm, l)
        PowerModels.constraint_thermal_limit_to(pm, l)
        if idx ∉ ref(pm, :region, region_idx, "in_arcs")
            PowerModels.constraint_voltage_angle_difference(pm, l)
        end
    end

    parameter_dual_variable(pm; report=true)
    expression_vanishing_measure(pm; report=true)
    objective_min_fuel_cost_dec(pm; report=true) ##
    expression_solution_comparison(pm; report=true)
end

function parameter_dual_variable(pm::ACRPowerModel; nw::Int=nw_id_default, report::Bool=false)
    beta_r = var(pm,nw)[:beta_r] = JuMP.@NLparameter(pm.model,
        beta_r[i in ids(pm, nw, :bus)] == 0.0
        )
    beta_i = var(pm,nw)[:beta_i] = JuMP.@NLparameter(pm.model,
        beta_i[i in ids(pm, nw, :bus)] == 0.0
        )
    memory_r = var(pm,nw)[:memory_r] = JuMP.@NLparameter(pm.model,
        memory_r[i in ids(pm, nw, :bus)] == 0.0
        )
    memory_i = var(pm,nw)[:memory_i] = JuMP.@NLparameter(pm.model,
        memory_i[i in ids(pm, nw, :bus)] == 0.0
        )

    if report
        beta_r_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], beta_r[i])
        beta_i_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], beta_i[i])
        memory_r_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], memory_r[i])
        memory_i_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], memory_i[i])

        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :beta_r, ids(pm, nw, :bus), beta_r_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :beta_i, ids(pm, nw, :bus), beta_i_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :memory_r, ids(pm, nw, :bus), memory_r_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :memory_i, ids(pm, nw, :bus), memory_i_ex)
    end
end
function parameter_dual_variable(pm::ACPPowerModel; nw::Int=nw_id_default, report::Bool=false)
    beta_m = var(pm,nw)[:beta_m] = JuMP.@NLparameter(pm.model,
        beta_m[i in ids(pm, nw, :bus)] == 0.0 )
    beta_a = var(pm,nw)[:beta_a] = JuMP.@NLparameter(pm.model,
        beta_a[i in ids(pm, nw, :bus)] == 0.0 )
    memory_m = var(pm,nw)[:memory_m] = JuMP.@NLparameter(pm.model,
        memory_m[i in ids(pm, nw, :bus)] == 0.0 )
    memory_a = var(pm,nw)[:memory_a] = JuMP.@NLparameter(pm.model,
        memory_a[i in ids(pm, nw, :bus)] == 0.0 )

    if report
        beta_m_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], beta_m[i])
        beta_a_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], beta_a[i])
        memory_m_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], memory_m[i])
        memory_a_ex = JuMP.@NLexpression(pm.model, [i in ids(pm, nw, :bus)], memory_a[i])

        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :beta_m, ids(pm, nw, :bus), beta_m_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :beta_a, ids(pm, nw, :bus), beta_a_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :memory_m, ids(pm, nw, :bus), memory_m_ex)
        _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :memory_a, ids(pm, nw, :bus), memory_a_ex)
    end
end

function expression_vanishing_measure(pm::AbstractPowerModel; nw::Int=nw_id_default, report::Bool=true)
    # Setting vanishing measure as a NL parameter for monitoring purposes
    vanish = var(pm,nw)[:vanish] = JuMP.@NLparameter(pm.model,
        vanish[i in ids(pm, nw, :bus)] == 0.0 )
    vanish_exp = Dict()
    for i in ids(pm,nw,:bus)
        vanish_exp[i] = JuMP.@NLexpression(pm.model, vanish[i])
    end
    report && _IM.sol_component_value(pm, pm_it_sym, nw, :bus, :vanish, ids(pm, nw, :bus), vanish_exp)
end


"
ADMM iteration
"
function update_voltage_copy_variables!(ds::ErsegheDS)
    for (k, lpm) in ds.pms
        for (j, bus) in ref(lpm, :bus)
            vkj = _read_complex_voltage(lpm, j)
            for h in bus["duplicated_regions"]
                lpm_h = ds.pms[h]
                lpm_h.copyVariables["voltage_prev"][j][k]     = lpm.copyVariables["voltage"][j][k]
                lpm_h.copyVariables["voltage"][j][k]          = vkj
            end
        end
    end
end
function initialize_voltage_copy_variables!(ds::ErsegheDS)
    for (k, lpm) in ds.pms
        for (j, bus) in ref(lpm, :bus)
            vkj = _read_complex_voltage_init(lpm, j)
            for h in bus["duplicated_regions"]
                lpm_h = ds.pms[h]
                lpm_h.copyVariables["voltage_prev"][j][k]     = lpm.copyVariables["voltage"][j][k]
                lpm_h.copyVariables["voltage"][j][k]          = vkj
            end
        end
    end
end


function update_dual_variables!(lpm::ErsegheLPM)
    for (nregion, region) in ref(lpm, :region)        # only one region per pm
        region_buses = region["neigh_bus"]
        for j in region_buses
            update_dual_variables!(lpm, j)
        end
    end
end
function update_dual_variables!(lpm::ErsegheLPM, j::Int)
    nregion = first(keys(ref(lpm,:region)))
    bus = ref(lpm, :bus, j)
    Mj = length(bus["duplicated_regions"])
    lpm.dualVariables["mixing_value_prev"][j] = lpm.dualVariables["mixing_value"][j]
    lpm.dualVariables["mixing_value"][j] = (Mj == 1) ? 0 :
                                        sum( 1/2*bus["param_a_tilde"][(nregion, h)]*
                                            (voltage_copy(lpm, j, nregion) - voltage_copy(lpm, j, h))
                                            for h in bus["duplicated_regions"])
    lpm.dualVariables["memory"][j] = lpm.dualVariables["memory"][j] + lpm.dualVariables["mixing_value"][j]
end
function update_dual_variables!(lpm::ErsegheLPM, j::Int, regs::Array{Int})
    nregion = first(keys(ref(lpm,:region)))
    bus = ref(lpm, :bus, j)
    Mj = length(bus["duplicated_regions"])
    lpm.dualVariables["mixing_value_prev"][j] = lpm.dualVariables["mixing_value"][j]
    lpm.dualVariables["mixing_value"][j] = (Mj == 1) ? 0 :
                                        sum( 1/2*bus["param_a_tilde"][(nregion, h)]*
                                            (voltage_copy(lpm, j, nregion) - voltage_copy(lpm, j, h))
                                            for h in bus["duplicated_regions"])
    temp = (Mj == 1) ? 0 :
                                        sum( 1/2*bus["param_a_tilde"][(nregion, h)]*
                                            (voltage_copy(lpm, j, nregion) - voltage_copy(lpm, j, h))
                                            for h in regs)
    lpm.dualVariables["memory"][j] = lpm.dualVariables["memory"][j] + temp
end


function initialize_dual_variables!(lpm::ErsegheLPM)
    for (nregion, region) in ref(lpm, :region)        # only one region per pm
        region_buses = region["neigh_bus"]
        for j in region_buses
            bus = ref(lpm, :bus, j)
            Mj = length(bus["duplicated_regions"])
            lpm.dualVariables["mixing_value_prev"][j] = lpm.dualVariables["mixing_value"][j]
            lpm.dualVariables["mixing_value"][j] = (Mj == 1) ? 0 :
                                                sum( 1/2*bus["param_a_tilde"][(nregion, h)]*
                                                    (voltage_copy(lpm, j, nregion) - voltage_copy(lpm, j, h))
                                                    for h in bus["duplicated_regions"])
            # lpm.dualVariables["memory"][j] = lpm.dualVariables["memory"][j] # already initialized with lpm initialization
        end
    end
end


function update_objective_functions!(lpm::ErsegheLPM)
    for j in ids(lpm,:bus)
        update_objective_functions!(lpm, j)
    end
end
function update_objective_functions!(lpm::ErsegheLPM, j::Int)
    k = lpm.region
    memory = lpm.dualVariables["memory"][j]
    beta_value = voltage_copy(lpm, j, k) - lpm.dualVariables["mixing_value"][j] - lpm.dualVariables["memory"][j]
    _set_beta(lpm, j, beta_value)
    _set_memory(lpm, j, memory)
end

_set_beta(lpm::ErsegheLPM, j::Int, beta_value::Complex) = _set_beta(lpm.pm, j, beta_value)
function _set_beta(pm::ACRPowerModel, j::Int, beta_value::Complex)
    beta_r = var(pm, :beta_r)
    beta_i = var(pm, :beta_i)

    JuMP.set_value(beta_r[j], real(beta_value))
    JuMP.set_value(beta_i[j], imag(beta_value))
end
function _set_beta(pm::ACPPowerModel, j::Int, beta_value::Complex)
    beta_m = var(pm, :beta_m)
    beta_a = var(pm, :beta_a)

    JuMP.set_value(beta_m[j], abs(beta_value))
    JuMP.set_value(beta_a[j], angle(beta_value))
end

_set_memory(lpm::ErsegheLPM, j::Int, memory_value::Complex) = _set_memory(lpm.pm, j, memory_value)
function _set_memory(pm::ACRPowerModel, j::Int, memory_value::Complex)
    memory_r = var(pm, :memory_r)
    memory_i = var(pm, :memory_i)

    JuMP.set_value(memory_r[j], real(memory_value))
    JuMP.set_value(memory_i[j], imag(memory_value))
end
function _set_memory(pm::ACPPowerModel, j::Int, memory_value::Complex)
    memory_m = var(pm, :memory_m)
    memory_a = var(pm, :memory_a)

    JuMP.set_value(memory_m[j], abs(memory_value))
    JuMP.set_value(memory_a[j], angle(memory_value))
end

function update_residuals!(ds::ErsegheDS)
    for (i, lpm) in ds.pms
        update_residuals!(lpm)
    end
    ds.vanishing_measure = compute_global_vanishing_measure(ds)
end
function update_residuals!(lpm::ErsegheLPM)
    terms = Dict()
    for (nregion, region) in ref(lpm,:region)            # only one region per pm
        for j in region["neigh_bus"]
            term = ref(lpm, :bus, j)["param_d"][nregion] * real(
                abs(voltage_copy(lpm, j)-voltage_copy_previous(lpm, j))^2
              + voltage_copy(lpm, j)'* lpm.dualVariables["mixing_value_prev"][j]
              + voltage_copy_previous(lpm, j)'*(
                    lpm.dualVariables["mixing_value"][j]-lpm.dualVariables["mixing_value_prev"][j]
                    )
                )
            terms[(nregion, j)] = term
            _set_vanish(lpm.pm, j, term)        # for monitoring reasons
        end
    end
    Γ = sum(x for (k,x) in terms)
    lpm.vanishingMeasure = Γ
end
function _set_vanish(pm::AbstractPowerModel, j::Int, value::Real)
    vanish = var(pm, :vanish)
    JuMP.set_value(vanish[j], value)
end
compute_global_vanishing_measure(ds::ErsegheDS) = sum(lpm.vanishingMeasure for (i,lpm) in ds.pms)


"
Solution building, creating one dictionary from the monitor of the decentralized ones.
"
function build_global_monitor(ds::ErsegheDS)
    result = Dict(  "bus"=>     Dict{String,Any}(),
                    "branch"=>  Dict{String,Any}(),
                    "gen"=>     Dict{String,Any}(),
                    "region"=>  Dict{String,Any}(),
                    )
    for (i,lpm) in ds.pms
        if lpm.monitoring
            region = collect(ids(lpm, :region))[1]
            build_global_monitor_erseghe(i, result, lpm.monitor, ref(lpm, :region, region, "bus"))
        end
    end
    return result
end
function build_global_monitor_erseghe(i::Int, result::Dict{String,<:Any}, 
    local_monitor::Dict{String,<:Any}, local_buses::AbstractVector)

    result["region"][string(i)] = Dict{String,Any}()
    result["region"][string(i)]["vanish"] = _regionwise_vanishing_measure(local_monitor)
    if haskey(local_monitor["bus"], "0")
        result["region"][string(i)]["voltage_comp"] = local_monitor["bus"]["0"]["comp_optimal"]
        result["region"][string(i)]["branch_comp"] = local_monitor["branch"]["0"]["comp_optimal"]
        result["region"][string(i)]["gen_comp"] = (haskey(local_monitor, "gen")) ? local_monitor["gen"]["0"]["comp_optimal"] : 0
    end

    for (key,dic) in local_monitor
        if key=="bus"
            for bus in local_buses
                result[key][string(bus)] = deepcopy(dic[string(bus)])
            end
        elseif (key == "iteration_counter") || (key == "local_iteration") || (key == "vanishingMeasureMA") || (key == "activeBus")
            result["region"][string(i)][key] = deepcopy(dic)
        else
            for (idx,vals) in dic
                if idx != "0"
                    if !haskey(result[key], idx)
                        result[key][idx] = deepcopy(vals)
                    else
                        for (k,v) in vals
                            result[key][idx][k] = v
                        end
                    end
                end
            end
        end
    end
end
function _regionwise_vanishing_measure(local_monitor::Dict{String,<:Any}) 
    vanishing_measure = sum(dic["vanish"] for (idx,dic) in local_monitor["bus"] if idx !="0")
    for (idx,dic) in local_monitor["bus"]
        delete!(dic, "vanish")
    end
    return vanishing_measure
end


