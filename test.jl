using PowerModels
# using DecentralizedPowerModels
include("src/DecentralizedPowerModels.jl")
using DataFrames
using JuMP, Ipopt

file = "test/data/matpower/case118_modifiedcost3_regions8.m"
model_type = PowerModels.ACPPowerModel
ipopt_solver = JuMP.optimizer_with_attributes(
	Ipopt.Optimizer, "tol"=>1e-6, "print_level"=>0)

result_ac_opf = PowerModels.run_opf(file, model_type, ipopt_solver; solution_processors=[])
objective = result_ac_opf["objective"]
result_ac_opf = result_ac_opf["solution"]

ds = instantiate_model_dec_erseghe(file, model_type, ipopt_solver);
result, monitor = run_dec_opf_erseghe(file, 
	model_type, ipopt_solver; 
	monitor_max_time=100);

# DecentralizedPowerModels.gif_monitor_gen_reactive(ds, monitor, result_ac_opf, name="test_gen.gif")


##
## region visualization
##

# b = DecentralizedPowerModels.regionsViz(file)
# a = DecentralizedPowerModels.linksDict(file)
# DecentralizedPowerModels.linksViz(file)
# DecentralizedPowerModels.linksRegionViz(ds)
# DecentralizedPowerModels.listRegionNeighbors(ds)
