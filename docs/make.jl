using DecentralizedPowerModels
using Documenter

makedocs(;
    modules=[DecentralizedPowerModels],
    authors="Alyssia Dong <alyssia.dong@ens-rennes.fr> and contributors",
    repo="https://gitlab.com/alyssiadong/DecentralizedPowerModels.jl/blob/{commit}{path}#L{line}",
    sitename="DecentralizedPowerModels.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://alyssiadong.gitlab.io/DecentralizedPowerModels.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
