# DecentralizedPowerModels

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://alyssiadong.gitlab.io/DecentralizedPowerModels.jl/dev)
[![Build Status](https://gitlab.com/alyssiadong/DecentralizedPowerModels.jl/badges/master/pipeline.svg)](https://gitlab.com/alyssiadong/DecentralizedPowerModels.jl/pipelines)
[![Coverage](https://gitlab.com/alyssiadong/DecentralizedPowerModels.jl/badges/master/coverage.svg)](https://gitlab.com/alyssiadong/DecentralizedPowerModels.jl/commits/master)


A wrapper around PowerModels.jl which allows the decentralization of the OPF problem using ADMM.

The first problem to be implemented is the one described in "Distributed optimal power flow using ADMM" by Tomaso Erseghe.

The second problem, named "B2B" for "bus to bus", is another ADMM distribution for the optimal power flow, except it uses branchwise decomposition for the voltage consensus.